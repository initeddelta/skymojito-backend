<?php
	/**
	 * Project: fly-tickets-aggregator
	 * File: AppExtension.php
	 * Author: Tomas SYROVY <tomas@syrovy.pro>
	 * Date: 22.11.17
	 * Version: 1.0
	 */

	namespace AppBundle\Twig;


	class AppExtension extends \Twig_Extension
	{
		public function getFilters()
		{
			return array(
				new \Twig_SimpleFilter('unserialize', array($this, 'unserialize')),
			);
		}

		public function unserialize($string)
		{
			return unserialize($string);
		}

		public function getName()
		{
			return 'app_extension';
		}


	}