<?php
	/**
	 * Project: fly-tickets-aggregator
	 * File: SocialPoster.php
	 * Author: Tomas SYROVY <tomas@syrovy.pro>
	 * Date: 07.12.17
	 * Version: 1.0
	 */

	namespace AppBundle\SocialPost;


	use AppBundle\AppBundle;
	use AppBundle\Entity\Post;
	use AppBundle\Entity\Social;
	use AppBundle\Entity\SocialPost;
	use Doctrine\ORM\Query\ResultSetMapping;
	use Doctrine\ORM\Query\ResultSetMappingBuilder;
	use Symfony\Component\Routing\Generator\UrlGenerator;
	use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

	class SocialPoster {

		/**
		 * @var \Doctrine\Common\Persistence\ObjectManager
		 */
		private $em;

		/**
		 * @var \Symfony\Component\HttpFoundation\Request
		 */
		private $request;

		/**
		 * @var \Symfony\Component\DependencyInjection\ContainerInterface
		 */
		private $container;

		/**
		 * SocialPoster constructor.
		 *
		 * @param \Doctrine\Common\Persistence\ObjectManager                $em
		 * @param \Symfony\Component\HttpFoundation\Request                 $request
		 * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
		 */
		public function __construct( \Doctrine\Common\Persistence\ObjectManager $em, \Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\DependencyInjection\ContainerInterface $container ){
			$this->em        = $em;
			$this->request   = $request;
			$this->container = $container;
		}

		/**
		 * @param string $code
		 *
		 * @return string
		 */
		public function publish($code = 'facebook_cs'){

			$social = $this->getSocial($code);

			if($social){

				if(($social->getType() === 'twitter' && AppBundle::$publishToTwitter) || ($social->getType() === 'facebook' && AppBundle::$publishToFacebook)){

					$post = $this->getPost($social);

					if($post && $social){

						$socialPost = $this->getSocialPost($post, $social);

						if($post->isFullValid() && !$socialPost){

							$params = [
								'post_id' => $post->getId()
							];
							$link = $this->container->get('router')->generate('app.default.post', $params, UrlGeneratorInterface::ABSOLUTE_URL);
							$pictureLink = $this->request->getScheme() . '://' . $this->request->getHttpHost() . $this->request->getBasePath()."/assets/destinations/".$post->getDestinationTo()->getImageName();

							$provider = new Provider($social);
							$published = $provider->publish($post, $link, $pictureLink);

							if($published){
								$socialPost = new SocialPost();
								$socialPost->setSocial($social);
								$socialPost->setPost($post);
								$socialPost->setCreatedAt(new \DateTime());
								$this->em->persist($socialPost);
								$this->em->flush();

								return '200:Successfull!';

							}

						}

					}

				}

			}

			return '500:NOT-Successfull!';

		}

		/**
		 * @param \AppBundle\Entity\Social $social
		 *
		 * @return \AppBundle\Entity\Post
		 */
		private function getPost(Social $social){

			/** @var \Doctrine\ORM\EntityManagerInterface $em */
			$em = $this->em;

			$sql = "
				SELECT p.id
				FROM post p
				  JOIN post_site ON p.id = post_site.post_id
				  JOIN site ON post_site.site_id = site.id
				  JOIN locale ON site.locale_id = locale.id
				WHERE locale.name = :locale_name
				  AND p.inappropriate = 0
				  AND p.created_at >= :created_at_after
				  AND p.id NOT IN (
				    SELECT post_id
				    FROM social_post
				      JOIN social ON social.id = social_post.social_id
				      WHERE social.code = :social_code
				  )
				  AND p.currency IS NOT NULL
				  AND p.price IS NOT NULL
				  AND p.destinationTo_id IS NOT NULL
				  AND p.destinationFrom_id IS NOT NULL
				ORDER BY p.created_at;
			";
			$rsm = new ResultSetMappingBuilder($this->em);
			$rsm->addRootEntityFromClassMetadata('AppBundle\Entity\Post', 'p');

			$created_at_after = new \DateTime();
			$created_at_after->sub(new \DateInterval('PT5H'));
			$created_at_after = $created_at_after->format('Y-m-d H:i:s');

			$query = $em->createNativeQuery($sql, $rsm);
			$query->setParameters([
				'locale_name' => $social->getLocale()->getName(),
				'created_at_after' => $created_at_after,
				'social_code' => $social->getCode()
			]);

			$result = $query->getResult();

			if(count($result) > 0 && array_key_exists(0, $result)){
//				dump($result[0]);exit;
				return $result[0];
			}

			return null;

		}

		/**
		 * @param $code
		 *
		 * @return \AppBundle\Entity\Social|null|object
		 */
		private function getSocial($code){

			$criteria = [
				'code' => $code
			];
			$social = $this->em->getRepository(Social::class)->findOneBy($criteria);

			return $social;

		}

		/**
		 * @param \AppBundle\Entity\Post   $post
		 * @param \AppBundle\Entity\Social $social
		 *
		 * @return \AppBundle\Entity\SocialPost|null|object
		 */
		private function getSocialPost(Post $post, Social $social){

			$criteria = [
				'post' => $post,
				'social' => $social
			];
			$socialPost = $this->em->getRepository(SocialPost::class)->findOneBy($criteria);

			return $socialPost;

		}

	}