<?php
	/**
	 * Project: fly-tickets-aggregator
	 * File: Provider.php
	 * Author: Tomas SYROVY <tomas@syrovy.pro>
	 * Date: 15.09.17
	 * Version: 1.0
	 */

	namespace AppBundle\SocialPost;


	use Abraham\TwitterOAuth\TwitterOAuth;
	use AppBundle\Entity\Post;
	use Facebook\Facebook;
	use MartinGeorgiev\SocialPost\Provider\Facebook\SDK5;
	use MartinGeorgiev\SocialPost\Provider\Message;
	use MartinGeorgiev\SocialPost\Provider\Twitter\TwitterOAuth07;

	class Provider {

		/**
		 * @var \AppBundle\Entity\Social
		 */
		private $social;

		/**
		 * Provider constructor.
		 *
		 * @param \AppBundle\Entity\Social $social
		 */
		public function __construct( \AppBundle\Entity\Social $social ){
			$this->social = $social;
		}

		public function getProvider(){

			switch($this->social->getType()){
				case 'facebook' : {
					$facebook = new Facebook($this->social->getConfig());
					$provider = new SDK5($facebook, $this->social->getPageId());
				}break;
				case 'twitter' : {
					$config = $this->social->getConfig();
					$consumerKey = $config['consumerKey'];
					$consumerSecret = $config['consumerKey'];
					$oauthToken = $config['oauthToken'];
					$oauthTokenSecret = $config['oauthTokenSecret'];
					$twitter = new TwitterOAuth($consumerKey, $consumerSecret, $oauthToken, $oauthTokenSecret);
					$provider = new TwitterOAuth07($twitter);
				}break;
				default : {
					throw new \Exception();
				}break;
			}

			return $provider;

		}

		public function publish(Post $post, $link, $pictureLink){

			$message = $post->getTitle($this->social->getLocale()->getName());

			$message = new Message($message, $link, $pictureLink);

			return $this->getProvider()->publish($message);

		}


	}