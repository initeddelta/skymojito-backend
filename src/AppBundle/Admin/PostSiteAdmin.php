<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class PostSiteAdmin extends AbstractAdmin
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
	        ->add('site', 'entity', [
		        'class' => 'AppBundle\Entity\Site',
		        'label' => 'Stránka',
		        'expanded' => false,
		        'multiple' => false,
		        'choice_label' => 'title',
		        'required' => true,
	        ])
	        ->add('url', 'text', [
		        'label' => 'URL (vč. http(s)://)'
	        ])
	        ->add('position', 'number', [
		        'label' => 'Pozice'
	        ])
        ;
    }
}
