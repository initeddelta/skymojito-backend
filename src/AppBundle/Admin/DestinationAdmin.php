<?php

    namespace AppBundle\Admin;

    use AppBundle\Entity\Destination;
    use AppBundle\Entity\Post;
    use Sonata\AdminBundle\Admin\AbstractAdmin;
    use Sonata\AdminBundle\Datagrid\DatagridMapper;
    use Sonata\AdminBundle\Datagrid\ListMapper;
    use Sonata\AdminBundle\Form\FormMapper;
    use Sonata\AdminBundle\Show\ShowMapper;

    class DestinationAdmin extends AbstractAdmin
    {

	    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
	    {
		    // this text filter will be used to retrieve autocomplete fields
		    $datagridMapper
			    ->add('title', 'doctrine_orm_callback', array(
			        'label' => 'Název',
				    'callback' => function($queryBuilder, $alias, $field, $value) {
					    if (!$value['value']) {
						    return;
					    }

					    $queryBuilder->leftJoin(sprintf('%s.destinationLocales', $alias), 'dl');
					    $queryBuilder->leftJoin('dl.locale', 'l');
					    $queryBuilder->andWhere('l.name = :name');
					    $queryBuilder->andWhere('dl.title LIKE :title');
					    $queryBuilder->setParameter('name', 'cs');
					    $queryBuilder->setParameter('title', '%'.$value['value'].'%');

					    return true;
				    },
				    'field_type' => 'text'
			    ))
		    ;
	    }

    	/**
         * @param ListMapper $listMapper
         */
        protected function configureListFields(ListMapper $listMapper)
        {
            $listMapper
                ->add('title', 'text', [
                    'label' => 'Název'
                ])
	            ->add('type', 'text', [
		            'label' => 'Typ'
	            ])
	            ->add('priority', 'number', [
		            'label' => 'Priorita'
	            ])
                ->add('_action', null, array(
                    'label' => 'Akce',
                    'actions' => array(
                        'edit' => array(),
                        'delete' => array(),
                    )
                ))
            ;
        }

        /**
         * @param FormMapper $formMapper
         */
        protected function configureFormFields(FormMapper $formMapper)
        {
            $formMapper
	            ->add('type', 'choice', [
		            'label' => 'Typ',
		            'choices' => [
		            	'01-continent' => '01-continent',
		            	'02-region' => '02-region',
		            	'03-subregion' => '03-subregion',
		            	'04-country' => '04-country',
		            	'05-city' => '05-city',
		            ]
	            ])
                ->add('priority', 'integer', [
                    'label' => 'Priorita',
                    'required' => false,
                ])
	            ->add('parent', 'sonata_type_model_autocomplete', [
		            'property' => 'title',
		            'label' => 'Nadřazený prvek',
//		            'expanded' => false,
//		            'multiple' => false,
//		            'choice_label' => 'title',
		            'required' => false,
	            ])
	            ->add('destinationLocales', 'sonata_type_collection', [
		            'by_reference' => false,
		            'label' => 'Popisky',
	            ],[
		            'edit' => 'inline',
		            'inline' => 'table'
	            ])
	            ->add('alternative', 'checkbox', [
		            'label' => 'Alternativní destinace',
		            'required' => false,
	            ])
	            ->add('alternativeParent', 'sonata_type_model_autocomplete', [
//		            'class' => 'AppBundle\Entity\Destination',
	                'property' => 'title',
		            'label' => 'Nadřazený prvek k alternativní destinaci',
//		            'expanded' => false,
//		            'multiple' => false,
//		            'choice_label' => 'title',
		            'required' => false,
	            ])
            ;
        }

        public function toString($object)
        {
	        return $object instanceof Destination
		        ? $object->getTitle()
		        : 'Destinace'; // shown in the breadcrumb on the create view
        }

    }
