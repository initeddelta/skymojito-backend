<?php

namespace AppBundle\Admin;

use AppBundle\Entity\Post;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

class AbstractPostAdmin extends AbstractAdmin
{

	/**
	 * @var array
	 */
	protected $datagridValues = array(

		// display the first page (default = 1)
		'_page' => 1,

		// reverse order (default = 'ASC')
		'_sort_order' => 'DESC',

		// name of the ordered field (default = the model's id field, if any)
		'_sort_by' => 'id',
	);

	/**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
	        ->add('id', null, [
		        'label' => 'Id'
	        ])
	        ->add('inappropriate', null, [
		        'label' => 'Nevyhodnotitelný'
	        ])
	        ->add('inappropriateReason', null, [
		        'label' => 'Příčina nevyhodnocení'
	        ])
	        ->add('maybePost.processed', null, [
		        'label' => 'Zpracovaný kandidát'
	        ])
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
	        ->add('inappropriate', 'checkbox', [
		        'label' => 'Nevyhodnocený'
	        ])
	        ->add('destinationTo', 'sonata_type_model_autocomplete', [
//		        'class' => 'AppBundle\Entity\Destination',
		        'label' => 'Destinace',
//		        'expanded' => false,
//		        'multiple' => false,
//		        'choice_label' => 'title',
		        'property' => 'title',
		        'required' => true,
	        ])
	        ->add('destinationFrom', 'sonata_type_model_autocomplete', [
//		        'class' => 'AppBundle\Entity\Destination',
		        'label' => 'Místo odletu',
//		        'expanded' => false,
//		        'multiple' => false,
//		        'choice_label' => 'title',
		        'property' => 'title',
		        'required' => true,
	        ])
	        ->add('price', 'number', [
		        'label' => 'Cena'
	        ])
	        ->add('currency', 'text', [
		        'label' => 'Měna'
	        ])
	        ->add('postLocales', 'sonata_type_collection', [
		        'by_reference' => false,
		        'label' => 'Texty',
	        ],[
		        'edit' => 'inline',
		        'inline' => 'table'
	        ])
	        ->add('postSites', 'sonata_type_collection', [
		        'by_reference' => false,
		        'label' => 'Odkazy',
	        ],[
		        'edit' => 'inline',
		        'inline' => 'table'
	        ])
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id', null, [
            	'label' => 'Id'
            ])
	        ->add('inappropriate', null, [
		        'label' => 'Nevyhodnocený'
	        ])
	        ->add('inappropriateReason', null, [
		        'label' => 'Příčina nevyhodnocení'
	        ])
	        ->add('destinationTo.title', null, [
		        'label' => 'Destinace'
	        ])
	        ->add('destinationFrom.title', null, [
		        'label' => 'Místo odletu'
	        ])
	        ->add('price', null, [
		        'label' => 'Cena'
	        ])
	        ->add('currency', null, [
		        'label' => 'Měna'
	        ])
	        ->add('url', null, [
		        'label' => 'URL'
	        ])
	        ->add('maybePost.id', null, [
		        'label' => 'Kandidát: Id'
	        ])
	        ->add('maybePost.maybeTo', null, [
		        'label' => 'Kandidát: Destinace'
	        ])
	        ->add('maybePost.maybeFrom', null, [
		        'label' => 'Kandidát: Místo odletu'
	        ])
	        ->add('maybePost.maybePrice', null, [
		        'label' => 'Kandidát: Cena'
	        ])
	        ->add('maybePost.maybeCurrency', null, [
		        'label' => 'Kandidát: Měna'
	        ])
	        ->add('maybePost.url', null, [
		        'label' => 'Kandidát: URL'
	        ])
	        ->add('maybePost.maybePageTo', null, [
		        'label' => 'Kandidát: Destinace (text)'
	        ])
	        ->add('maybePost.maybePageFrom', null, [
		        'label' => 'Kandidát: Místo odletu (text)'
	        ])
        ;
    }

	public function toString($object)
	{
		return $object instanceof Post
			? $object->getDestinationTo()->getTitle()
			: 'Příspěvky'; // shown in the breadcrumb on the create view
	}

}
