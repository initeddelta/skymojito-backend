<?php

namespace AppBundle\Admin;

use AppBundle\Entity\MaybePost;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

class MaybePostAdmin extends AbstractAdmin
{

	/**
	 * @var array
	 */
	protected $datagridValues = array(

		// display the first page (default = 1)
		'_page' => 1,

		// reverse order (default = 'ASC')
		'_sort_order' => 'DESC',

		// name of the ordered field (default = the model's id field, if any)
		'_sort_by' => 'id',
	);

	protected function configureRoutes(RouteCollection $collection)
	{
		$collection->clearExcept(array('list', 'show'));
	}




	public function getListModes(){

		$listModes = parent::getListModes();
		unset($listModes['mosaic']);

		return $listModes;
	}

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
	        ->add('processed', null, [
		        'label' => 'Zpracováno'
	        ])
	        ->add('maybeTo', null, [
		        'label' => 'Destinace'
	        ])
	        ->add('maybeFrom', null, [
		        'label' => 'Místo odletu'
	        ])
	        ->add('maybePrice', null, [
		        'label' => 'Cena'
	        ])
	        ->add('maybeCurrency', null, [
		        'label' => 'Měna'
	        ])
	        ->add('url', null, [
		        'label' => 'URL'
	        ])
	        ->add('site.title', null, [
		        'label' => 'Stránka'
	        ])
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
	        ->add('id', null, [
		        'label' => 'Id'
	        ])
	        ->add('processed', null, [
	        	'label' => 'Zpracováno'
	        ])
	        ->add('maybeTo', null, [
		        'label' => 'Destinace'
	        ])
            ->add('maybeFrom', null, [
	            'label' => 'Místo odletu'
            ])
	        ->add('maybePrice', null, [
		        'label' => 'Cena'
	        ])
	        ->add('maybeCurrency', null, [
		        'label' => 'Měna'
	        ])
	        ->add('url', null, [
		        'label' => 'URL'
	        ])
	        ->add('site.title', null, [
		        'label' => 'Stránka'
	        ])
	        ->add('createdAt', null, [
		        'label' => 'Vytvořeno'
	        ])
            ->add('_action', null, array(
            	'label' => 'Akce',
                'actions' => array(
                    'show' => array(),
                )
            ))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
	        ->add('id', null, [
		        'label' => 'Id'
	        ])
	        ->add('processed', null, [
		        'label' => 'Zpracováno'
	        ])
	        ->add('maybeTo', null, [
		        'label' => 'Destinace'
	        ])
	        ->add('maybeFrom', null, [
		        'label' => 'Místo odletu'
	        ])
	        ->add('maybePrice', null, [
		        'label' => 'Cena'
	        ])
	        ->add('maybeCurrency', null, [
		        'label' => 'Měna'
	        ])
	        ->add('url', null, [
		        'label' => 'URL'
	        ])
	        ->add('site.title', null, [
		        'label' => 'Stránka'
	        ])
	        ->add('createdAt', null, [
		        'label' => 'Vytvořeno'
	        ])
	        ->add('maybePageTo', null, [
		        'label' => 'Destinace (text)'
	        ])
	        ->add('maybePageFrom', null, [
		        'label' => 'Místo odletu (text)'
	        ])
        ;
    }

	public function toString($object)
	{
		return $object instanceof MaybePost
			? $object->getId()
			: 'Kandidáti na příspěvky'; // shown in the breadcrumb on the create view
	}
}
