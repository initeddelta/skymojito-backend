<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class PostInappropriatedAdmin extends AbstractPostAdmin
{

	protected $baseRouteName = 'inappropriate';
	protected $baseRoutePattern = 'inappropriate';

	public function createQuery($context = 'list'){

		$proxyQuery = parent::createQuery($context);
		//Default alias is "o"
		$proxyQuery->where('o.inappropriate = :inappropriate');
		$proxyQuery->setParameter('inappropriate', true);

		return $proxyQuery;

	}

	/**
	 * @param ListMapper $listMapper
	 */
	protected function configureListFields(ListMapper $listMapper)
	{
		$listMapper
			->add('inappropriateReason', null, [
				'label' => 'Příčina nevyhodnocení'
			])
			->add('destinationTo.title', null, [
				'label' => 'Destinace'
			])
			->add('destinationFrom.title', null, [
				'label' => 'Místo odletu'
			])
			->add('price', null, [
				'label' => 'Cena'
			])
			->add('currency', null, [
				'label' => 'Měna'
			])
			->add('url', null, [
				'label' => 'URL'
			])
			->add('createdAt', null, [
				'label' => 'Vytvořeno'
			])
			->add('_action', null, array(
				'actions' => array(
					'show' => array(),
					'edit' => array(),
					'delete' => array(),
				)
			))
		;
	}

}
