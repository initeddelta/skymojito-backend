<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class DestinationLocaleAdmin extends AbstractAdmin
{
	/**
	 * @param FormMapper $formMapper
	 */
	protected function configureFormFields(FormMapper $formMapper)
	{
		$formMapper
			->add('locale', 'entity', [
				'class' => 'AppBundle\Entity\Locale',
				'label' => 'Jazyk',
				'expanded' => false,
				'multiple' => false,
				'choice_label' => 'name',
				'required' => true,
			])
			->add('title', 'text', [
				'label' => 'Název',
			])
			->add('info', 'sonata_simple_formatter_type', [
				'label' => 'Text',
				'format' => 'richhtml',
				'ckeditor_context' => 'default', // optional
			])
		;
	}
}
