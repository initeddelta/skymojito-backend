<?php
/**
 * Project: jak-ziji-cesi-app
 * File: NotificationManager.php
 * Author: Tomas SYROVY <tomas@syrovy.pro>
 * Date: 26.10.16
 * Version: 1.0
 */

namespace AppBundle\NotificationManager;


use AppBundle\Entity\Notification;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use FOS\UserBundle\Mailer\MailerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Translation\TranslatorInterface;

class NotificationManager {

	/**
	 * @var ContainerInterface
	 */
	private $container;

	/**
	 * @var Controller
	 */
	private $controller;

	/**
	 * @var EntityManagerInterface
	 */
	private $entityManager;

	/**
	 * @var TranslatorInterface
	 */
	private $translator;

	/**
	 * @var \Swift_Mailer
	 */
	private $mailer;

	/**
	 * NotificationManager constructor.
	 *
	 * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
	 * @param \Symfony\Bundle\FrameworkBundle\Controller\Controller     $controller
	 * @param \Doctrine\ORM\EntityManagerInterface                      $entityManager
	 * @param \Symfony\Component\Translation\TranslatorInterface        $translator
	 * @param \Swift_Mailer                                             $mailer
	 */
	public function __construct( \Symfony\Component\DependencyInjection\ContainerInterface $container, \Symfony\Bundle\FrameworkBundle\Controller\Controller $controller, \Doctrine\ORM\EntityManagerInterface $entityManager, \Symfony\Component\Translation\TranslatorInterface $translator, \Swift_Mailer $mailer ){
		$this->container     = $container;
		$this->controller    = $controller;
		$this->entityManager = $entityManager;
		$this->translator    = $translator;
		$this->mailer        = $mailer;
	}


	/**
	 * @param string $type
	 * @param array $recipients
	 * @param string $view
	 *
	 */
	public function notify($type, $recipients, $view){

		$subject = $this->translator->trans(NotificationTypeLibrary::$NOTIFICATION_TYPES[$type]['subject']);

		$notification = new Notification();
		$notification->setSubject($subject);
		$notification->setSenderEmail($this->container->getParameter('sender_email'));
		$notification->setSenderName($this->container->getParameter('sender_name'));
		$notification->setRecipients($recipients);
		$notification->setBodyHtml($view);
		$notification->setCreatedAt(new \DateTime());

		$message = \Swift_Message::newInstance();
		$message->setSubject($notification->getSubject());
		$message->setFrom($notification->getSenderEmail(), $notification->getSenderName());
		$message->setBcc($notification->getRecipients());
		$message->setBody($notification->getBodyHtml(), 'text/html');

		$this->entityManager->persist($notification);
		$this->entityManager->flush();

		return $this->mailer->send($message);

	}


}