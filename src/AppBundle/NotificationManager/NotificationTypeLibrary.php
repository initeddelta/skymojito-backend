<?php
	/**
	 * File: NotificationTypeLibrary.php
	 * Author: Tomas SYROVY <tomas@syrovy.pro>
	 * Date: 26.10.16
	 * Version: 1.0
	 */

	namespace AppBundle\NotificationManager;


	class NotificationTypeLibrary {

		public static $NOTIFICATION_TYPES = [
			'notification' => [
				'subject' => 'Nová akce na SkyMojito',
				'template' => '@App/email/notification.html.twig'
	       ],
		];

	}
