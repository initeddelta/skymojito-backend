<?php

	namespace AppBundle\Command;

	use AppBundle\Service\Crawler;
	use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
	use Symfony\Component\Console\Input\InputInterface;
	use Symfony\Component\Console\Output\OutputInterface;

	class CrawlerCommand extends ContainerAwareCommand
	{
		protected function configure()
		{
			$this
				->setName('app:crawler:run')
				->setDescription('Run crawling over all sites.')
			;
		}

		protected function execute(InputInterface $input, OutputInterface $output)
		{
			/** @var Crawler $crawler */
			$crawler = $this->getContainer()->get('app.crawler');
			$crawler->run();

//			$text = 'Done! All sites has been crawled.';
//
//			$output->writeln($text);
		}
	}