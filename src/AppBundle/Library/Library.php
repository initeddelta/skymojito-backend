<?php
	/**
	 * Project: fly-tickets-aggregator
	 * File: Library.php
	 * Author: Tomas SYROVY <tomas@syrovy.pro>
	 * Date: 06.02.18
	 * Version: 1.0
	 */

	namespace AppBundle\Library;


	class Library {

		//JE NUTNE PSAT VSECHNY MOZNE PADY PRO KAZDE SLOVO V KAZDE MUTACI

		//Nevyhodnocuj tato slova
		public static $badWords = [
			'air',
			'airways',
			'airline',
			'airlines',
			'tap', //tap portugal
			'ostrov',
			'island',
			'eiland',
			'insel',
			'wyspa',
			'остров',
			'per',
			'for',
		];

		//Nevyhodnocuj celý příspěvěk, pokud v nadpisu obsahuje
		public static $forbiddenWords = [
			'hotel'
		];

	}