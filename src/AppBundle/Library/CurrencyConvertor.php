<?php
	/**
	 * Project: fly-tickets-aggregator
	 * File: CurrencyConvertor.php
	 * Author: Tomas SYROVY <tomas@syrovy.pro>
	 * Date: 01.02.18
	 * Version: 1.0
	 */

	namespace AppBundle\Library;


	class CurrencyConvertor {

		public static $currencies = [
			'CZK', 'EUR', 'PLN', 'RUB', 'GBP',
		];

		public static function getRates($domainCurrency){

		    $rates = [];
		    foreach(self::$currencies as $currency){
		        $rates[$currency] = 1;
            }
            $url = "http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml";
            $data = file_get_contents($url);
            $xml = simplexml_load_string($data);
            /** @var \SimpleXMLElement $cube */
            foreach($xml->Cube->Cube->Cube as $cube){
                $attributes = $cube->attributes();
                $currency = (string)$attributes['currency'];
                $rate = (float)$attributes['rate'];
                if(in_array($currency, self::$currencies, false)){
                    $rates[$currency] = $rate;
                }
            }

            $rts = [];
            foreach(self::$currencies as $currency){
                $rts[$currency] = $rates[$domainCurrency]/$rates[$currency];
            }

			return $rts;

		}

	}