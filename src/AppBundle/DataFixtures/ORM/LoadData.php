<?php

	namespace AppBundle\DataFixtures\ORM;

	use AppBundle\Entity\Destination;
	use AppBundle\Entity\DestinationFrom;
	use AppBundle\Entity\DestinationLocale;
	use AppBundle\Entity\Locale;
	use AppBundle\Entity\Social;
	use AppBundle\Factory\SiteFactory;
	use Doctrine\Common\DataFixtures\FixtureInterface;
	use Doctrine\Common\Persistence\ObjectManager;
	use simplehtmldom_1_5\simple_html_dom;
	use Symfony\Component\Debug\Exception\ContextErrorException;
	use Symfony\Component\Finder\Finder;

	class LoadData implements FixtureInterface
	{
		private static $objects = [];

		private static $locales = [
			'cs', 'en', 'de', 'pl', 'sk', 'ru'
		];

		public function load(ObjectManager $manager)
		{

			ini_set('memory_limit', '-1');

//			$siteFactory = new SiteFactory();
//			$data = $this->getData('site.json');
//			$dataJSON = json_decode($data, true);
//			foreach($dataJSON['entities'] as $options){
//
//				self::$objects[] = $siteFactory->create( $options );
//
//			}

			$this->generateSocials();

//			$this->generateLocale();
//			$this->generateDestinationFroms();
//
//			$this->loadFromExcel($manager);

			$this->persistAll($manager);
			$manager->flush();
		}

		private function generateSocials(){

			$s1 = new Social();
			$s1->setCode('facebook_cs');
			$s1->setType('facebook');
			$s1->setPageId('1910993835831319');
			$s1->setConfig([
				'app_id' => '354847978286682',
				'app_secret' => '7e06290421bfa031739aebafafb25ed3',
				'default_access_token' => 'EAAFCu3ux7loBAC4CDqbwHhcj6pMSdPB7DQyzDsFJZCnC7qUTZAcKab42xxguAcBqiI0CxTuBXhjZCKIhTykD3Au3cFoPqBjW0bKLXRZBadVopNvmyJMEweixZBICZBZC1uVWzD7D5roi0SoAcUhAYZBtz8txCsvusQpKZCLOQQlK6rAZDZD'
			]);

			self::$objects['s1'] = $s1;

			$data = file_get_contents(__DIR__.'/social.json');
			$d = json_decode($data, true);
			foreach($d['socials'] as $social){

				$s = new Social();
				$s->setCode($social['code']);
				$s->setType($social['type']);
				$s->setConfig($social['config']);

				self::$objects[$social['code']] = $s;

			}

		}

		private function generateLocale(){

			foreach(self::$locales as $locale){

				$object = new Locale();
				$object->setName( $locale );

				self::$objects['locale_'.$locale] = $object;

			}

		}

		/**
		 * @param ObjectManager $manager
		 */
		private function generateDestinationFroms(){

			$destinationFroms = [
				'cs' => [
					'Česko',
					'Česko a okolí',
					'Evropa',
					'odkudkoliv'
				],
				'sk' => [
					'Slovensko',
					'Slovensko a okolie',
					'Európa',
					'odkiaľkoľvek'
				],
				'en' => [
					'Great Britain',
					'Western Europe',
					'Europe',
					'from anywhere'
				],
				'de' => [
					'Deutschland',
					'Österreich',
					'Deutschland und Umgebung',
					'Österreich und Umgebung',
					'von überall'
				],
				'pl' => [
					'Polska',
					'Polska i okolica',
					'Europa',
					'skądkolwiek'
				],
				'ru' => [
					'Россия',
					'Восточная Европа',
					'Европа',
					'из любого места'
				],
			];

			foreach($destinationFroms as $keyLocale => $destinations){

				foreach($destinations as $destination){

					$df = new DestinationFrom();
					$df->setTitle($destination);
					$df->setLocale(self::$objects['locale_'.$keyLocale]); //toto musí být objekt!!

					self::$objects['df_'.$destination] = $df;

				}

			}

		}

		private function getData($fileName){

			$finder = new Finder();

			foreach($finder->in(__DIR__)->name($fileName) as $file){
				return $file->getContents();
			}

		}

		private function loadFromExcel(ObjectManager $manager){

			$rowMin = 4;
			$rowMax = 1035;

			$col_continentCZ = 1;
			$col_regionCZ = 2;
			$col_subregionCZ = 3;
			$col_countryCZ = 4;
			$col_countryCZ_alt = 5;
			$col_cityCZ = 6;
			$col_cityCZ_alt = 7;
			$col_cityCZ_alt2 = 8;
			$col_image1 = 11;
			$col_image2 = 12;
			$col_image3 = 13;
			$col_priority = 14;
			$col_df_Cesko = 15;
			$col_df_CeskoAOkoli = 16;
			$col_df_Evropa = 17;
			$col_df_odkudkoliv = 18;
			$col_df_zemeSK = 19;
			$col_df_okoliSK = 20;
			$col_df_zonaSK = 21;
			$col_df_odkudkolivSK = 22;
			$col_df_zemePL = 23;
			$col_df_okoliPL = 24;
			$col_df_zonaPL = 25;
			$col_df_odkudkolivPL = 26;
			$col_df_zemeGB = 27;
			$col_df_okoliGB = 28;
			$col_df_zonaGB = 29;
			$col_df_odkudkolivGB = 30;
			$col_df_zemeDE = 31;
			$col_df_zemeAT = 32;
			$col_df_okoliDE = 33;
			$col_df_okoliAT = 34;
			$col_df_odkudkolivDEAT = 35;
			$col_df_zemeRU = 36;
			$col_df_okoliRU = 37;
			$col_df_zonaRU = 38;
			$col_df_odkudkolivRU = 39;
			
			$col_continentSK = 40;
			$col_regionSK = 41;
			$col_subregionSK = 42;
			$col_countrySK = 43;
			$col_countrySK_alt = 44;
			$col_citySK = 45;
			$col_citySK_alt = 46;
			$col_citySK_alt2 = 47;

			$col_continentEN = 48;
			$col_regionEN = 49;
			$col_subregionEN = 50;
			$col_countryEN = 51;
			$col_countryEN_alt = 52;
			$col_cityEN = 53;
			$col_cityEN_alt = 54;
			$col_cityEN_alt2 = 55;

			$col_continentDE = 56;
			$col_regionDE = 57;
			$col_subregionDE = 58;
			$col_countryDE = 59;
			$col_countryDE_alt = 60;
			$col_cityDE = 61;
			$col_cityDE_alt = 62;
			$col_cityDE_alt2 = 63;

			$col_continentPL = 64;
			$col_regionPL = 65;
			$col_subregionPL = 66;
			$col_countryPL = 67;
			$col_countryPL_alt = 68;
			$col_cityPL = 69;
			$col_cityPL_alt = 70;
			$col_cityPL_alt2 = 71;

			$col_continentRU = 72;
			$col_regionRU = 73;
			$col_subregionRU = 74;
			$col_countryRU = 75;
			$col_countryRU_alt = 76;
			$col_cityRU = 77;
			$col_cityRU_alt = 78;
			$col_cityRU_alt2 = 79;


			$filename = __DIR__.'/airports2.xlsx';
			$phpExcelObject = \PHPExcel_IOFactory::load($filename);

			$sheet = $phpExcelObject->getActiveSheet();

			//Lvl 1 A
			for($pRow = $rowMin; $pRow <= $rowMax; $pRow++){

				$value = $sheet->getCellByColumnAndRow($col_continentCZ, $pRow)->getValue();

				if($value and !empty($value) and $value !== null and !array_key_exists(strtolower($value), self::$objects)){

					$object = new Destination();
					foreach(self::$locales as $locale){
						switch($locale){
							case 'cs' : {
								$title = $value;
							}break;
							case 'sk' : {
								$title = $sheet->getCellByColumnAndRow($col_continentSK, $pRow)->getValue();
							}break;
							case 'en' : {
								$title = $sheet->getCellByColumnAndRow($col_continentEN, $pRow)->getValue();
							}break;
							case 'pl' : {
								$title = $sheet->getCellByColumnAndRow($col_continentPL, $pRow)->getValue();
							}break;
							case 'de' : {
								$title = $sheet->getCellByColumnAndRow($col_continentDE, $pRow)->getValue();
							}break;
							case 'ru' : {
								$title = $sheet->getCellByColumnAndRow($col_continentRU, $pRow)->getValue();
							}break;
							default : {
								$title = $value;
							}break;

						}
						if(!$title or empty($title) or $title === null or array_key_exists(strtolower('dl_'.$locale.'_'.$title), self::$objects)){
							continue;
						}
						$objectL = new DestinationLocale();
						$objectL->setDestination($object);
						$objectL->setLocale(self::$objects['locale_'.$locale]);
						$objectL->setTitle($title);
						self::$objects['dl_'.$locale.'_'.$title] = $objectL;
					}
					$image1 = $sheet->getCellByColumnAndRow($col_image1, $pRow)->getValue();
					if($image1 and !empty($image1) and $image1 !== null){
						$object->setImageName1($image1);
					}
					$image2 = $sheet->getCellByColumnAndRow($col_image2, $pRow)->getValue();
					if($image2 and !empty($image2) and $image2 !== null){
						$object->setImageName2($image2);
					}
					$image3 = $sheet->getCellByColumnAndRow($col_image3, $pRow)->getValue();
					if($image3 and !empty($image3) and $image3 !== null){
						$object->setImageName3($image3);
					}
					$object->setType('01-continent');
					$priority = $sheet->getCellByColumnAndRow($col_priority, $pRow)->getValue();
					if(!$priority){
						$priority = 999;
					}
					$object->setPriority($priority);

					$df = $sheet->getCellByColumnAndRow($col_df_Cesko, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Česko']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_CeskoAOkoli, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Česko a okolí']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_Evropa, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Evropa']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_odkudkoliv, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_odkudkoliv']);
					}

					$df = $sheet->getCellByColumnAndRow($col_df_zemeSK, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Slovensko']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_okoliSK, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Slovensko a okolie']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_zonaSK, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Európa']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_odkudkolivSK, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_odkiaľkoľvek']);
					}

					$df = $sheet->getCellByColumnAndRow($col_df_zemePL, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Polska']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_okoliPL, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Polska i okolica']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_zonaPL, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Europa']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_odkudkolivPL, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_skądkolwiek']);
					}

					$df = $sheet->getCellByColumnAndRow($col_df_zemeGB, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Great Britain']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_okoliGB, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Western Europe']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_zonaGB, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Europe']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_odkudkolivGB, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_from anywhere']);
					}

					$df = $sheet->getCellByColumnAndRow($col_df_zemeDE, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Deutschland']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_zemeAT, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Österreich']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_okoliDE, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Deutschland und Umgebung']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_okoliAT, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Österreich und Umgebung']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_odkudkolivDEAT, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_von überall']);
					}

					$df = $sheet->getCellByColumnAndRow($col_df_zemeRU, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Россия']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_okoliRU, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Восточная Европа']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_zonaRU, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Европа']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_odkudkolivRU, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_из любого места']);
					}

					$index = strtolower($value);
					self::$objects[$index] = $object;

				}

			}

			//Lvl 2 B
			for($pRow = $rowMin; $pRow <= $rowMax; $pRow++){

				$value = $sheet->getCellByColumnAndRow($col_regionCZ, $pRow)->getValue();

				if($value and !empty($value) and $value !== null and !array_key_exists(strtolower($value), self::$objects)){

					$object = new Destination();
					foreach(self::$locales as $locale){
						switch($locale){
							case 'cs' : {
								$title = $value;
							}break;
							case 'sk' : {
								$title = $sheet->getCellByColumnAndRow($col_regionSK, $pRow)->getValue();
							}break;
							case 'en' : {
								$title = $sheet->getCellByColumnAndRow($col_regionEN, $pRow)->getValue();
							}break;
							case 'pl' : {
								$title = $sheet->getCellByColumnAndRow($col_regionPL, $pRow)->getValue();
							}break;
							case 'de' : {
								$title = $sheet->getCellByColumnAndRow($col_regionDE, $pRow)->getValue();
							}break;
							case 'ru' : {
								$title = $sheet->getCellByColumnAndRow($col_regionRU, $pRow)->getValue();
							}break;
							default : {
								$title = $value;
							}break;

						}
						if(!$title or empty($title) or $title === null or array_key_exists(strtolower('dl_'.$locale.'_'.$title), self::$objects)){
							continue;
						}
						$objectL = new DestinationLocale();
						$objectL->setDestination($object);
						$objectL->setLocale(self::$objects['locale_'.$locale]);
						$objectL->setTitle($title);
						self::$objects['dl_'.$locale.'_'.$title] = $objectL;
					}
					$image1 = $sheet->getCellByColumnAndRow($col_image1, $pRow)->getValue();
					if($image1 and !empty($image1) and $image1 !== null){
						$object->setImageName1($image1);
					}
					$image2 = $sheet->getCellByColumnAndRow($col_image2, $pRow)->getValue();
					if($image2 and !empty($image2) and $image2 !== null){
						$object->setImageName2($image2);
					}
					$image3 = $sheet->getCellByColumnAndRow($col_image3, $pRow)->getValue();
					if($image3 and !empty($image3) and $image3 !== null){
						$object->setImageName3($image3);
					}
					$object->setType('02-region');
					$priority = $sheet->getCellByColumnAndRow($col_priority, $pRow)->getValue();
					if(!$priority){
						$priority = 999;
					}
					$object->setPriority($priority);

					$df = $sheet->getCellByColumnAndRow($col_df_Cesko, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Česko']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_CeskoAOkoli, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Česko a okolí']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_Evropa, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Evropa']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_odkudkoliv, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_odkudkoliv']);
					}

					$df = $sheet->getCellByColumnAndRow($col_df_zemeSK, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Slovensko']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_okoliSK, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Slovensko a okolie']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_zonaSK, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Európa']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_odkudkolivSK, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_odkiaľkoľvek']);
					}

					$df = $sheet->getCellByColumnAndRow($col_df_zemePL, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Polska']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_okoliPL, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Polska i okolica']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_zonaPL, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Europa']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_odkudkolivPL, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_skądkolwiek']);
					}

					$df = $sheet->getCellByColumnAndRow($col_df_zemeGB, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Great Britain']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_okoliGB, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Western Europe']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_zonaGB, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Europe']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_odkudkolivGB, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_from anywhere']);
					}

					$df = $sheet->getCellByColumnAndRow($col_df_zemeDE, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Deutschland']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_zemeAT, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Österreich']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_okoliDE, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Deutschland und Umgebung']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_okoliAT, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Österreich und Umgebung']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_odkudkolivDEAT, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_von überall']);
					}

					$df = $sheet->getCellByColumnAndRow($col_df_zemeRU, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Россия']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_okoliRU, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Восточная Европа']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_zonaRU, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Европа']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_odkudkolivRU, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_из любого места']);
					}

					$valueParent = $sheet->getCellByColumnAndRow($col_continentCZ, $pRow)->getValue();
					if($valueParent and !empty($valueParent) and $valueParent !== null and array_key_exists(strtolower($valueParent), self::$objects)){
						$parent = self::$objects[strtolower($valueParent)];

						$object->setParent($parent);
					}

					$index = strtolower($value);
					self::$objects[$index] = $object;

				}

			}

			//Lvl 3 C
			for($pRow = $rowMin; $pRow <= $rowMax; $pRow++){

				$value = $sheet->getCellByColumnAndRow($col_subregionCZ, $pRow)->getValue();

				if($value and !empty($value) and $value !== null and !array_key_exists(strtolower($value), self::$objects)){

					$object = new Destination();
					foreach(self::$locales as $locale){
						switch($locale){
							case 'cs' : {
								$title = $value;
							}break;
							case 'sk' : {
								$title = $sheet->getCellByColumnAndRow($col_subregionSK, $pRow)->getValue();
							}break;
							case 'en' : {
								$title = $sheet->getCellByColumnAndRow($col_subregionEN, $pRow)->getValue();
							}break;
							case 'pl' : {
								$title = $sheet->getCellByColumnAndRow($col_subregionPL, $pRow)->getValue();
							}break;
							case 'de' : {
								$title = $sheet->getCellByColumnAndRow($col_subregionDE, $pRow)->getValue();
							}break;
							case 'ru' : {
								$title = $sheet->getCellByColumnAndRow($col_subregionRU, $pRow)->getValue();
							}break;
							default : {
								$title = $value;
							}break;

						}
						if(!$title or empty($title) or $title === null or array_key_exists(strtolower('dl_'.$locale.'_'.$title), self::$objects)){
							continue;
						}
						$objectL = new DestinationLocale();
						$objectL->setDestination($object);
						$objectL->setLocale(self::$objects['locale_'.$locale]);
						$objectL->setTitle($title);
						self::$objects['dl_'.$locale.'_'.$title] = $objectL;
					}
					$image1 = $sheet->getCellByColumnAndRow($col_image1, $pRow)->getValue();
					if($image1 and !empty($image1) and $image1 !== null){
						$object->setImageName1($image1);
					}
					$image2 = $sheet->getCellByColumnAndRow($col_image2, $pRow)->getValue();
					if($image2 and !empty($image2) and $image2 !== null){
						$object->setImageName2($image2);
					}
					$image3 = $sheet->getCellByColumnAndRow($col_image3, $pRow)->getValue();
					if($image3 and !empty($image3) and $image3 !== null){
						$object->setImageName3($image3);
					}
					$object->setType('03-subregion');
					$priority = $sheet->getCellByColumnAndRow($col_priority, $pRow)->getValue();
					if(!$priority){
						$priority = 999;
					}
					$object->setPriority($priority);

					$df = $sheet->getCellByColumnAndRow($col_df_Cesko, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Česko']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_CeskoAOkoli, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Česko a okolí']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_Evropa, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Evropa']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_odkudkoliv, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_odkudkoliv']);
					}

					$df = $sheet->getCellByColumnAndRow($col_df_zemeSK, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Slovensko']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_okoliSK, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Slovensko a okolie']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_zonaSK, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Európa']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_odkudkolivSK, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_odkiaľkoľvek']);
					}

					$df = $sheet->getCellByColumnAndRow($col_df_zemePL, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Polska']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_okoliPL, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Polska i okolica']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_zonaPL, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Europa']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_odkudkolivPL, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_skądkolwiek']);
					}

					$df = $sheet->getCellByColumnAndRow($col_df_zemeGB, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Great Britain']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_okoliGB, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Western Europe']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_zonaGB, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Europe']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_odkudkolivGB, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_from anywhere']);
					}

					$df = $sheet->getCellByColumnAndRow($col_df_zemeDE, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Deutschland']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_zemeAT, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Österreich']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_okoliDE, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Deutschland und Umgebung']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_okoliAT, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Österreich und Umgebung']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_odkudkolivDEAT, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_von überall']);
					}

					$df = $sheet->getCellByColumnAndRow($col_df_zemeRU, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Россия']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_okoliRU, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Восточная Европа']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_zonaRU, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Европа']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_odkudkolivRU, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_из любого места']);
					}

					$valueParent = $sheet->getCellByColumnAndRow($col_regionCZ, $pRow)->getValue();
					if($valueParent and !empty($valueParent) and $valueParent !== null and array_key_exists(strtolower($valueParent), self::$objects)){
						$parent = self::$objects[strtolower($valueParent)];

						$object->setParent($parent);
					}

					$index = strtolower($value);
					self::$objects[$index] = $object;

				}

			}

			//Lvl 4 D
			for($pRow = $rowMin; $pRow <= $rowMax; $pRow++){

				$value = $sheet->getCellByColumnAndRow($col_countryCZ, $pRow)->getValue();

				if($value and !empty($value) and $value !== null){

					$object = new Destination();
					foreach(self::$locales as $locale){
						switch($locale){
							case 'cs' : {
								$title = $value;
							}break;
							case 'sk' : {
								$title = $sheet->getCellByColumnAndRow($col_countrySK, $pRow)->getValue();
							}break;
							case 'en' : {
								$title = $sheet->getCellByColumnAndRow($col_countryEN, $pRow)->getValue();
							}break;
							case 'pl' : {
								$title = $sheet->getCellByColumnAndRow($col_countryPL, $pRow)->getValue();
							}break;
							case 'de' : {
								$title = $sheet->getCellByColumnAndRow($col_countryDE, $pRow)->getValue();
							}break;
							case 'ru' : {
								$title = $sheet->getCellByColumnAndRow($col_countryRU, $pRow)->getValue();
							}break;
							default : {
								$title = $value;
							}break;

						}
						if(!$title or empty($title) or $title === null or array_key_exists(strtolower('dl_'.$locale.'_'.$title), self::$objects)){
							continue;
						}
						$objectL = new DestinationLocale();
						$objectL->setDestination($object);
						$objectL->setLocale(self::$objects['locale_'.$locale]);
						$objectL->setTitle($title);
						self::$objects['dl_'.$locale.'_'.$title] = $objectL;
					}
					$image1 = $sheet->getCellByColumnAndRow($col_image1, $pRow)->getValue();
					if($image1 and !empty($image1) and $image1 !== null){
						$object->setImageName1($image1);
					}
					$image2 = $sheet->getCellByColumnAndRow($col_image2, $pRow)->getValue();
					if($image2 and !empty($image2) and $image2 !== null){
						$object->setImageName2($image2);
					}
					$image3 = $sheet->getCellByColumnAndRow($col_image3, $pRow)->getValue();
					if($image3 and !empty($image3) and $image3 !== null){
						$object->setImageName3($image3);
					}
					$object->setType('04-country');
					$priority = $sheet->getCellByColumnAndRow($col_priority, $pRow)->getValue();
					if(!$priority){
						$priority = 999;
					}
					$object->setPriority($priority);

					$df = $sheet->getCellByColumnAndRow($col_df_Cesko, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Česko']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_CeskoAOkoli, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Česko a okolí']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_Evropa, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Evropa']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_odkudkoliv, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_odkudkoliv']);
					}

					$df = $sheet->getCellByColumnAndRow($col_df_zemeSK, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Slovensko']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_okoliSK, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Slovensko a okolie']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_zonaSK, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Európa']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_odkudkolivSK, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_odkiaľkoľvek']);
					}

					$df = $sheet->getCellByColumnAndRow($col_df_zemePL, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Polska']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_okoliPL, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Polska i okolica']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_zonaPL, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Europa']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_odkudkolivPL, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_skądkolwiek']);
					}

					$df = $sheet->getCellByColumnAndRow($col_df_zemeGB, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Great Britain']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_okoliGB, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Western Europe']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_zonaGB, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Europe']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_odkudkolivGB, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_from anywhere']);
					}

					$df = $sheet->getCellByColumnAndRow($col_df_zemeDE, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Deutschland']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_zemeAT, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Österreich']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_okoliDE, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Deutschland und Umgebung']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_okoliAT, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Österreich und Umgebung']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_odkudkolivDEAT, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_von überall']);
					}

					$df = $sheet->getCellByColumnAndRow($col_df_zemeRU, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Россия']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_okoliRU, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Восточная Европа']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_zonaRU, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Европа']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_odkudkolivRU, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_из любого места']);
					}

					$valueParent = $sheet->getCellByColumnAndRow($col_subregionCZ, $pRow)->getValue();
					if($valueParent and !empty($valueParent) and $valueParent !== null and array_key_exists(strtolower($valueParent), self::$objects)){
						$parent = self::$objects[strtolower($valueParent)];

						$object->setParent($parent);
					}

					$index = strtolower($value);
					self::$objects[$index] = $object;

				}

			}

			//Lvl 4b E
			for($pRow = $rowMin; $pRow <= $rowMax; $pRow++){

				$value = $sheet->getCellByColumnAndRow($col_countryCZ_alt, $pRow)->getValue();

				if($value and !empty($value) and $value !== null){

					$valueParent = $sheet->getCellByColumnAndRow($col_countryCZ, $pRow)->getValue();
					if($valueParent and !empty($valueParent) and $valueParent !== null and array_key_exists(strtolower($valueParent), self::$objects)){
						$parent = self::$objects[strtolower($valueParent)];

						$object = new Destination();
						foreach(self::$locales as $locale){
							switch($locale){
								case 'cs' : {
									$title = $value;
								}break;
								case 'sk' : {
									$title = $sheet->getCellByColumnAndRow($col_countrySK_alt, $pRow)->getValue();
								}break;
								case 'en' : {
									$title = $sheet->getCellByColumnAndRow($col_countryEN_alt, $pRow)->getValue();
								}break;
								case 'pl' : {
									$title = $sheet->getCellByColumnAndRow($col_countryPL_alt, $pRow)->getValue();
								}break;
								case 'de' : {
									$title = $sheet->getCellByColumnAndRow($col_countryDE_alt, $pRow)->getValue();
								}break;
								case 'ru' : {
									$title = $sheet->getCellByColumnAndRow($col_countryRU_alt, $pRow)->getValue();
								}break;
								default : {
									$title = $value;
								}break;

							}
							if(!$title or empty($title) or $title === null or array_key_exists(strtolower('dl_'.$locale.'_'.$title), self::$objects)){
								continue;
							}
							$objectL = new DestinationLocale();
							$objectL->setDestination($object);
							$objectL->setLocale(self::$objects['locale_'.$locale]);
							$objectL->setTitle($title);
							self::$objects['dl_'.$locale.'_'.$title] = $objectL;
						}
						$object->setAlternative(true);
						$object->setAlternativeParent($parent);
						$object->setPriority($parent->getPriority());

						$index = strtolower($value);
						self::$objects[$index] = $object;

					}

				}

			}

			//Lvl 5 F
			for($pRow = $rowMin; $pRow <= $rowMax; $pRow++){

				$value = $sheet->getCellByColumnAndRow($col_cityCZ, $pRow)->getValue();

				if($value and !empty($value) and $value !== null){

					$object = new Destination();
					foreach(self::$locales as $locale){
						switch($locale){
							case 'cs' : {
								$title = $value;
							}break;
							case 'sk' : {
								$title = $sheet->getCellByColumnAndRow($col_citySK, $pRow)->getValue();
							}break;
							case 'en' : {
								$title = $sheet->getCellByColumnAndRow($col_cityEN, $pRow)->getValue();
							}break;
							case 'pl' : {
								$title = $sheet->getCellByColumnAndRow($col_cityPL, $pRow)->getValue();
							}break;
							case 'de' : {
								$title = $sheet->getCellByColumnAndRow($col_cityDE, $pRow)->getValue();
							}break;
							case 'ru' : {
								$title = $sheet->getCellByColumnAndRow($col_cityRU, $pRow)->getValue();
							}break;
							default : {
								$title = $value;
							}break;

						}
						if(!$title or empty($title) or $title === null or array_key_exists(strtolower('dl_'.$locale.'_'.$title), self::$objects)){
							continue;
						}
						$objectL = new DestinationLocale();
						$objectL->setDestination($object);
						$objectL->setLocale(self::$objects['locale_'.$locale]);
						$objectL->setTitle($title);
						self::$objects['dl_'.$locale.'_'.$title] = $objectL;
					}
					$image1 = $sheet->getCellByColumnAndRow($col_image1, $pRow)->getValue();
					if($image1 and !empty($image1) and $image1 !== null){
						$object->setImageName1($image1);
					}
					$image2 = $sheet->getCellByColumnAndRow($col_image2, $pRow)->getValue();
					if($image2 and !empty($image2) and $image2 !== null){
						$object->setImageName2($image2);
					}
					$image3 = $sheet->getCellByColumnAndRow($col_image3, $pRow)->getValue();
					if($image3 and !empty($image3) and $image3 !== null){
						$object->setImageName3($image3);
					}
					$object->setType('05-city');
					$priority = $sheet->getCellByColumnAndRow($col_priority, $pRow)->getValue();
					if(!$priority){
						$priority = 999;
					}
					$object->setPriority($priority);

					$df = $sheet->getCellByColumnAndRow($col_df_Cesko, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Česko']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_CeskoAOkoli, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Česko a okolí']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_Evropa, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Evropa']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_odkudkoliv, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_odkudkoliv']);
					}

					$df = $sheet->getCellByColumnAndRow($col_df_zemeSK, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Slovensko']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_okoliSK, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Slovensko a okolie']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_zonaSK, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Európa']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_odkudkolivSK, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_odkiaľkoľvek']);
					}

					$df = $sheet->getCellByColumnAndRow($col_df_zemePL, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Polska']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_okoliPL, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Polska i okolica']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_zonaPL, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Europa']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_odkudkolivPL, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_skądkolwiek']);
					}

					$df = $sheet->getCellByColumnAndRow($col_df_zemeGB, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Great Britain']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_okoliGB, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Western Europe']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_zonaGB, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Europe']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_odkudkolivGB, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_from anywhere']);
					}

					$df = $sheet->getCellByColumnAndRow($col_df_zemeDE, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Deutschland']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_zemeAT, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Österreich']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_okoliDE, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Deutschland und Umgebung']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_okoliAT, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Österreich und Umgebung']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_odkudkolivDEAT, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_von überall']);
					}

					$df = $sheet->getCellByColumnAndRow($col_df_zemeRU, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Россия']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_okoliRU, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Восточная Европа']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_zonaRU, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_Европа']);
					}
					$df = $sheet->getCellByColumnAndRow($col_df_odkudkolivRU, $pRow)->getValue();
					if($df == 'ANO'){
						$object->addDestinationFrom(self::$objects['df_из любого места']);
					}

					$valueParent = $sheet->getCellByColumnAndRow($col_countryCZ, $pRow)->getValue();
					if($valueParent and !empty($valueParent) and $valueParent !== null and array_key_exists(strtolower($valueParent), self::$objects)){
						$parent = self::$objects[strtolower($valueParent)];

						$object->setParent($parent);
					}

					$index = strtolower($value);
					self::$objects[$index] = $object;

				}

			}

			//Lvl 5b G
			for($pRow = $rowMin; $pRow <= $rowMax; $pRow++){

				$value = $sheet->getCellByColumnAndRow($col_cityCZ_alt, $pRow)->getValue();

				if($value and !empty($value) and $value !== null){

					$valueParent = $sheet->getCellByColumnAndRow($col_cityCZ, $pRow)->getValue();
					if($valueParent and !empty($valueParent) and $valueParent !== null and array_key_exists(strtolower($valueParent), self::$objects)){
						$parent = self::$objects[strtolower($valueParent)];

						$object = new Destination();
						foreach(self::$locales as $locale){
							switch($locale){
								case 'cs' : {
									$title = $value;
								}break;
								case 'sk' : {
									$title = $sheet->getCellByColumnAndRow($col_citySK_alt, $pRow)->getValue();
								}break;
								case 'en' : {
									$title = $sheet->getCellByColumnAndRow($col_cityEN_alt, $pRow)->getValue();
								}break;
								case 'pl' : {
									$title = $sheet->getCellByColumnAndRow($col_cityPL_alt, $pRow)->getValue();
								}break;
								case 'de' : {
									$title = $sheet->getCellByColumnAndRow($col_cityDE_alt, $pRow)->getValue();
								}break;
								case 'ru' : {
									$title = $sheet->getCellByColumnAndRow($col_cityRU_alt, $pRow)->getValue();
								}break;
								default : {
									$title = $value;
								}break;

							}
							if(!$title or empty($title) or $title === null or array_key_exists(strtolower('dl_'.$locale.'_'.$title), self::$objects)){
								continue;
							}
							$objectL = new DestinationLocale();
							$objectL->setDestination($object);
							$objectL->setLocale(self::$objects['locale_'.$locale]);
							$objectL->setTitle($title);
							self::$objects['dl_'.$locale.'_'.$title] = $objectL;
						}
						$object->setAlternative(true);
						$object->setAlternativeParent($parent);
						$object->setPriority($parent->getPriority());

						$index = strtolower($value);
						self::$objects[$index] = $object;

					}

				}

			}

			//Lvl 5c H
			for($pRow = $rowMin; $pRow <= $rowMax; $pRow++){

				$value = $sheet->getCellByColumnAndRow($col_cityCZ_alt2, $pRow)->getValue();

				if($value and !empty($value) and $value !== null){

					$valueParent = $sheet->getCellByColumnAndRow($col_cityCZ, $pRow)->getValue();
					if($valueParent and !empty($valueParent) and $valueParent !== null and array_key_exists(strtolower($valueParent), self::$objects)){
						$parent = self::$objects[strtolower($valueParent)];

						$object = new Destination();
						foreach(self::$locales as $locale){
							switch($locale){
								case 'cs' : {
									$title = $value;
								}break;
								case 'sk' : {
									$title = $sheet->getCellByColumnAndRow($col_citySK_alt2, $pRow)->getValue();
								}break;
								case 'en' : {
									$title = $sheet->getCellByColumnAndRow($col_cityEN_alt2, $pRow)->getValue();
								}break;
								case 'pl' : {
									$title = $sheet->getCellByColumnAndRow($col_cityPL_alt2, $pRow)->getValue();
								}break;
								case 'de' : {
									$title = $sheet->getCellByColumnAndRow($col_cityDE_alt2, $pRow)->getValue();
								}break;
								case 'ru' : {
									$title = $sheet->getCellByColumnAndRow($col_cityRU_alt2, $pRow)->getValue();
								}break;
								default : {
									$title = $value;
								}break;

							}
							if(!$title or empty($title) or $title === null or array_key_exists(strtolower('dl_'.$locale.'_'.$title), self::$objects)){
								continue;
							}
							$objectL = new DestinationLocale();
							$objectL->setDestination($object);
							$objectL->setLocale(self::$objects['locale_'.$locale]);
							$objectL->setTitle($title);
							self::$objects['dl_'.$locale.'_'.$title] = $objectL;
						}
						$object->setAlternative(true);
						$object->setAlternativeParent($parent);
						$object->setPriority($parent->getPriority());

						$index = strtolower($value);
						self::$objects[$index] = $object;

					}

				}

			}

		}

		private function persistAll(ObjectManager $manager){

			foreach(self::$objects as $object){
				$manager->persist($object);
			}

		}

	}