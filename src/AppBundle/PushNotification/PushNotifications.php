<?php

	/**
	 * Created by PhpStorm.
	 * User: hofmanix
	 * Date: 04/04/2017
	 * Time: 13:11
	 */

	namespace AppBundle\PushNotification;

	use AppBundle\Entity\Device;

	class PushNotifications
	{

		/**
		 * @var \Doctrine\Common\Persistence\ObjectManager
		 */
		private $em;

		// serverovy API klic pro Android
		// ziskava se ve google FCM dev konzoli
		private static $API_ACCESS_KEY = 'AAAAOqcVq-o:APA91bHC0j-sMTEpxa1jzgsJk0C1pTmjebss1r8IGQ7nhN9ISxa2PqGQXGR1dCcvwJ43jjhN8Q7obAn3Z9EwRu8uidxleSyg6eiDNRc18bFOhUArxGf7NNTSQ-NegWw8O6q30V5TJsuj';

		/**
		 * PushNotifications constructor.
		 *
		 * @param \Doctrine\Common\Persistence\ObjectManager $em
		 */
		public function __construct( \Doctrine\Common\Persistence\ObjectManager $em ){
			$this->em = $em;
		}

		//Registers phone to send message
		//Kód potřebuje být upraven aby updatoval správné políčko s guid, následující kód byl původně upraven pro wordpress a vzhledem k tomu, že nevím, jaký systém se bude používat, netroufám si ho natolik upravit,
		//Myslím si ale, že i tak je kód velice vypovídající
		public function register($data) {

			$criteria = [
				'pnToken' => $data['pnToken']
			];
			$entity = $this->em->getRepository(Device::class)->findOneBy($criteria);

			if($entity){
				$entity->setUser($data['user']);
				$entity->setRegTime($data['regTime']);
			}else{
				$entity = new Device();
				$entity->setRegTime($data['regTime']);
				$entity->setPnToken($data['pnToken']);
				$entity->setUser($data['user']);
				$entity->setType($data['type']);
				$entity->setModel($data['model']);
				$entity->setOs($data['os']);
			}

			$this->em->persist($entity);
			$this->em->flush();

			return $entity;

		}

		//Vybere z databáze všechny tokeny, roztřídí je dle typu os a pošle na ně zprávu
		public function sendToAll($title, $message, $id, $type, $actionsCount = null) {

			$registrations = $this->em->getRepository(Device::class)->findAll();

			//Action id je nově přidaná nepovinná položka s id příspěvku, která se má zobrazit -> při typu single
			//Type, nepovinná položka obsahující typ notifikace dle analýzy -> multiple/single
			//Actions count, nepovinná položka obsahující počet nových akcí -> při typu multiple
			$data = array(
				"mtitle" => $title,
				"mdesc" => $message,
				"actionId" => $id,
				"type" => $type,
				"actionsCount" => $actionsCount
			);

			$androidKeys = array();
			$iosKeys = array();

			foreach($registrations as $registration) {
				if($registration->getType() == "android") {
					$androidKeys[] = $registration->getPnToken();
				} else if ($registration->getType() == "ios") {
					$iosKeys[] = $registration->getPnToken();
				} else {
				}
			}

			if(count($androidKeys) > 0) {
				$response = $this->android($data, $androidKeys);
				$this->logResponse($androidKeys, $response);
			}
			if(count($iosKeys) > 0) {
				$response = $this->iOS($data, $iosKeys);
				$this->logResponse($iosKeys, $response);
			}

		}

		private function android($data, $reg_ids)
		{
			$url = 'https://fcm.googleapis.com/fcm/send';
			$message = array(
				'title' => $data['mtitle'],
				'message' => $data['mdesc'],
				'subtitle' => '',
				'tickerText' => '',
//                'msgcnt' => 1,
				'vibrate' => 1,
				'type' => $data['type'],
				'actionId' => $data['actionId'],
				'actionsCount' => $data['actionsCount']
			);

			$headers = array(
				'Authorization: key=' . self::$API_ACCESS_KEY,
				'Content-Type: application/json'
			);

			$fields = array(
				'registration_ids' => $reg_ids,
				'data' => $message
			);

			return $this->useCurl($url, $headers, json_encode($fields));
		}

		// Sends Push notification for iOS users
		private function iOS($data, $devicetokens)
		{

			$results = array();
			foreach($devicetokens as $devicetoken) {
				$results[] = $this->sendIOS($data, $devicetoken);
			}
			return $results;
		}

		private function sendIOS($data, $devicetoken) {
			try {
				//$ctx = stream_context_create();
				$ctx = stream_context_create(array('ssl' => array(
					'verify_peer' => false,
					'cafile' => null,
					'local_cert' => __DIR__.'/../../../web/apn.pem',
				)));

				// ck.pem is your certificate file
				//stream_context_set_option($ctx, 'ssl', 'local_cert', 'apn.cer');
//            stream_context_set_option($ctx, 'ssl', 'Heslo123', self::$passphrase);
				// Open a connection to the APNS server
//                'ssl://gateway.sandbox.push.apple.com:2195', $err,
				$fp = stream_socket_client(
					'ssl://gateway.sandbox.push.apple.com:2195', $err,
					$errstr, 60, STREAM_CLIENT_CONNECT, $ctx);
				if (!$fp) {
					//exit("Failed to connect: $err $errstr" . PHP_EOL);
					throw new \Exception("Failed to connect: $err $errstr" . PHP_EOL);
				}
				// Create the payload body
				$body['aps'] = array(
					'alert' => array(
						'title' => $data['mtitle'],
						'body' => $data['mdesc']
					),
					'sound' => 'default',
					'content-available' => 1,
					'type' => $data['type'], //TODO - zjistit
					'actionId' => $data['actionId'], //TODO - zjistit
					'actionsCount' => $data['actionsCount'] //TODO - zjistit
				);
				// Encode the payload as JSON
				$payload = json_encode($body);
				// Build the binary notification
				$msg = chr(0) . pack('n', 32) . pack('H*', $devicetoken) . pack('n', strlen($payload)) . $payload;
				// Send it to the server
				$result = fwrite($fp, $msg, strlen($msg));
				$resultArr = array();
				if (!$result)
					$resultArr = array("token" => $devicetoken, "status" => 'Message not delivered');
				else
					$resultArr = array("token" => $devicetoken, "status" => 'Message successfully delivered', "result" => $result);

				// Close the connection to the server
				fclose($fp);

			} catch (\Exception $ex) {
				return array("status" => "Error while delivering package", "exception" => $ex, "message" => $ex->getMessage());
			}

			return $resultArr;
		}

		// Curl
		private function useCurl($url, $headers, $fields = null)
		{
			// Open connection
			$ch = curl_init();
			if ($url) {
				// Set the url, number of POST vars, POST data
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

				// Disabling SSL Certificate support temporarly
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				if ($fields) {
					curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
				}

				// Execute post
				$result = curl_exec($ch);
				if ($result === FALSE) {
					return "Curl failed";
					//die('Curl failed: ' . curl_error($ch));
				}

				// Close connection
				curl_close($ch);

				return $result;
			}
		}

		private function logResponse($keys, $response) {
			$filename = __DIR__.'/../../../var/logs/log.txt';
			$file = fopen($filename, "a");
			$date = date("Y-m-d H:i:s");
			$keys = json_encode($keys);
			$response = json_encode($response);
			fwrite($file, $date . " " . $keys . " " . $response . PHP_EOL);
			fclose($file);
		}
	}