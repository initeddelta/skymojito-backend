<?php

namespace AppBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class AppBundle extends Bundle
{
	public static $publishToTwitter = false;
	public static $publishToFacebook = false;
}
