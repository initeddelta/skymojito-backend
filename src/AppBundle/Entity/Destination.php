<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;

/**
 * Destination
 *
 * @Gedmo\Tree(type="closure")
 * @Gedmo\TreeClosure(class="AppBundle\Entity\DestinationClosure")
 * @ORM\Table(name="destination")
 * @ORM\Entity(repositoryClass="Gedmo\Tree\Entity\Repository\ClosureTreeRepository")
 */
class Destination
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

	/**
	 * @var \Doctrine\Common\Collections\ArrayCollection
	 *
	 * @ORM\OneToMany(targetEntity="AppBundle\Entity\DestinationLocale", mappedBy="destination", cascade={"persist", "remove"})
	 */
	private $destinationLocales;

    /**
     * This parameter is optional for the closure strategy
     *
     * @ORM\Column(name="level", type="integer", nullable=true)
     * @Gedmo\TreeLevel
     */
    private $level;

    /**
     * @Gedmo\TreeParent
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     * @ORM\ManyToOne(targetEntity="Destination", inversedBy="children", cascade={"persist"})
     */
    private $parent;

    /**
     * @ORM\Column(name="image_name1", type="string", length=64, nullable=true)
     */
    private $imageName1;

	/**
	 * @ORM\Column(name="image_name2", type="string", length=64, nullable=true)
	 */
	private $imageName2;

	/**
	 * @ORM\Column(name="image_name3", type="string", length=64, nullable=true)
	 */
	private $imageName3;

	/**
	 * @ORM\Column(name="image_name4", type="string", length=64, nullable=true)
	 */
	private $imageName4;

    /**
     * @ORM\Column(name="priority", type="integer")
     */
    private $priority = 999;

	/**
	 * @ORM\Column(name="type", type="string", length=64, nullable=true)
	 */
    private $type;

	/**
	 * @var bool
	 *
	 * @ORM\Column(name="alternative", type="boolean")
	 */
    private $alternative = false;

	/**
	 * @var \AppBundle\Entity\Destination
	 *
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Destination", cascade={"persist"})
	 * @ORM\JoinColumn(referencedColumnName="id")
	 */
    private $alternativeParent;

	/**
	 * @var \Doctrine\Common\Collections\ArrayCollection
	 *
	 * @ORM\ManyToMany(targetEntity="AppBundle\Entity\DestinationFrom", inversedBy="destinations", cascade={"persist"})
	 */
    private $destinationFroms;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\DestinationTo", inversedBy="destinations", cascade={"persist"})
     */
    private $destinationTos;

    public function getTag(){
        $url = $this->getTitle('cs');
        $url = preg_replace('~[^\\pL0-9_]+~u', '-', $url);
        $url = trim($url, "-");
        $url = iconv("utf-8", "us-ascii//TRANSLIT", $url);
        $url = strtolower($url);
        $url = preg_replace('~[^-a-z0-9_]+~', '', $url);
        return 'tag_'.$url;
    }

    public function __toString(){
	    return (string)$this->getTitle();
    }

	public function getTitle($localeName = 'cs'){

    	$firstTitle = null;
    	$title = null;

    	if($this->getDestinationLocales()->count() !== 0 ){
		    foreach($this->getDestinationLocales() as $destinationLocale){
		    	$firstTitle = $destinationLocale->getTitle();
			    if($destinationLocale->getLocale()->getName() === $localeName){
				    $title = $destinationLocale->getTitle();
			    }
		    }
	    }

	    if($title === null){
    		return $firstTitle;
	    }

	    return $title;

    }

    public function getImageName(){

    	$arr = [];
    	if($this->imageName1 !== null && file_exists(__DIR__.'/../../../web/assets/destinations/'.$this->imageName1)){
    		$arr[] = $this->imageName1;
	    }
	    if($this->imageName2 !== null && file_exists(__DIR__.'/../../../web/assets/destinations/'.$this->imageName2)){
		    $arr[] = $this->imageName2;
	    }
	    if($this->imageName3 !== null && file_exists(__DIR__.'/../../../web/assets/destinations/'.$this->imageName3)){
		    $arr[] = $this->imageName3;
	    }
	    if($this->imageName4 !== null && file_exists(__DIR__.'/../../../web/assets/destinations/'.$this->imageName4)){
		    $arr[] = $this->imageName4;
	    }

	    if(count($arr) === 0){
	    	return '';
	    }

	    return $arr[random_int(0, count($arr)-1)];

    }

    public function getId()
    {
        return $this->id;
    }

    public function setParent(Destination $parent = null)
    {
        $this->parent = $parent;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function addClosure(DestinationClosure $closure)
    {
        $this->closures[] = $closure;
    }

    public function setLevel($level)
    {
        $this->level = $level;
    }

    public function getLevel()
    {
        return $this->level;
    }

    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * Set imageName
     *
     * @param string $imageName
     *
     * @return Destination
     */
    public function setImageName1($imageName)
    {

	    $lastDotPos = strrpos($imageName, '.');
	    if($lastDotPos){
	    	$extension = substr($imageName, $lastDotPos+1);
	    	if(in_array(strtolower($extension), ['jpg', 'jpeg', 'png'], false)){
			    $this->imageName1 = $imageName;
		    }
	    }

        return $this;
    }

    /**
     * Get imageName
     *
     * @return string
     */
    public function getImageName1()
    {
        return $this->imageName1;
    }



    /**
     * Set priority
     *
     * @param integer $priority
     *
     * @return Destination
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority
     *
     * @return integer
     */
    public function getPriority()
    {
        return $this->priority;
    }

	/**
	 * @return mixed
	 */
	public function getType(){
		return $this->type;
	}

	/**
	 * @param mixed $type
	 */
	public function setType( $type ){
		$this->type = $type;
	}

	/**
	 * @return bool
	 */
	public function isAlternative(){
		return $this->alternative;
	}

	/**
	 * @param bool $alternative
	 */
	public function setAlternative( $alternative ){
		$this->alternative = $alternative;
	}

	/**
	 * @return \AppBundle\Entity\Destination
	 */
	public function getAlternativeParent(){
		return $this->alternativeParent;
	}

	/**
	 * @param \AppBundle\Entity\Destination $alternativeParent
	 */
	public function setAlternativeParent( $alternativeParent ){
		$this->alternativeParent = $alternativeParent;
	}

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->destinationFroms = new \Doctrine\Common\Collections\ArrayCollection();
        $this->destinationTos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->destinationLocales = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get alternative
     *
     * @return boolean
     */
    public function getAlternative()
    {
        return $this->alternative;
    }

    /**
     * Add destinationFrom
     *
     * @param \AppBundle\Entity\DestinationFrom $destinationFrom
     *
     * @return Destination
     */
    public function addDestinationFrom(\AppBundle\Entity\DestinationFrom $destinationFrom)
    {
        $this->destinationFroms[] = $destinationFrom;
//        $destinationFrom->addDestination($this);

        return $this;
    }

    /**
     * Remove destinationFrom
     *
     * @param \AppBundle\Entity\DestinationFrom $destinationFrom
     */
    public function removeDestinationFrom(\AppBundle\Entity\DestinationFrom $destinationFrom)
    {
        $this->destinationFroms->removeElement($destinationFrom);
    }

    /**
     * Get destinationFroms
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDestinationFroms()
    {
        return $this->destinationFroms;
    }

    /**
     * Add destinationTo
     *
     * @param \AppBundle\Entity\DestinationTo $destinationTo
     *
     * @return Destination
     */
    public function addDestinationTo(\AppBundle\Entity\DestinationTo $destinationTo)
    {
        $this->destinationTos[] = $destinationTo;
//        $destinationTo->addDestination($this);

        return $this;
    }

    /**
     * Remove destinationTo
     *
     * @param \AppBundle\Entity\DestinationTo $destinationTo
     */
    public function removeDestinationTo(\AppBundle\Entity\DestinationTo $destinationTo)
    {
        $this->destinationTos->removeElement($destinationTo);
    }

    /**
     * Get destinationTos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDestinationTos()
    {
        return $this->destinationTos;
    }

    /**
     * Set imageName2
     *
     * @param string $imageName
     *
     * @return Destination
     */
    public function setImageName2($imageName)
    {
	    $lastDotPos = strrpos($imageName, '.');
	    if($lastDotPos){
		    $extension = substr($imageName, $lastDotPos+1);
		    if(in_array(strtolower($extension), ['jpg', 'jpeg', 'png'], false)){
			    $this->imageName2 = $imageName;
		    }
	    }

        return $this;
    }

    /**
     * Get imageName2
     *
     * @return string
     */
    public function getImageName2()
    {
        return $this->imageName2;
    }

    /**
     * Set imageName3
     *
     * @param string $imageName
     *
     * @return Destination
     */
    public function setImageName3($imageName)
    {
	    $lastDotPos = strrpos($imageName, '.');
	    if($lastDotPos){
		    $extension = substr($imageName, $lastDotPos+1);
		    if(in_array(strtolower($extension), ['jpg', 'jpeg', 'png'], false)){
			    $this->imageName3 = $imageName;
		    }
	    }

        return $this;
    }

    /**
     * Get imageName3
     *
     * @return string
     */
    public function getImageName3()
    {
        return $this->imageName3;
    }

    /**
     * Set imageName4
     *
     * @param string $imageName
     *
     * @return Destination
     */
    public function setImageName4($imageName)
    {
	    $lastDotPos = strrpos($imageName, '.');
	    if($lastDotPos){
		    $extension = substr($imageName, $lastDotPos+1);
		    if(in_array(strtolower($extension), ['jpg', 'jpeg', 'png'], false)){
			    $this->imageName4 = $imageName;
		    }
	    }

        return $this;
    }

    /**
     * Get imageName4
     *
     * @return string
     */
    public function getImageName4()
    {
        return $this->imageName4;
    }

    /**
     * Add destinationLocale
     *
     * @param \AppBundle\Entity\DestinationLocale $destinationLocale
     *
     * @return Destination
     */
    public function addDestinationLocale(\AppBundle\Entity\DestinationLocale $destinationLocale)
    {
        $this->destinationLocales[] = $destinationLocale;

        return $this;
    }

    /**
     * Remove destinationLocale
     *
     * @param \AppBundle\Entity\DestinationLocale $destinationLocale
     */
    public function removeDestinationLocale(\AppBundle\Entity\DestinationLocale $destinationLocale)
    {
        $this->destinationLocales->removeElement($destinationLocale);
    }

    /**
     * Get destinationLocales
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDestinationLocales()
    {
        return $this->destinationLocales;
    }
}
