<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MaybePost
 *
 * @ORM\Table(name="maybe_post")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MaybePostRepository")
 */
class MaybePost
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Site
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Site")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $site;

    /**
     * @var string
     *
     * @ORM\Column(name="maybeFrom", type="text", nullable=true)
     */
    private $maybeFrom;

    /**
     * @var string
     *
     * @ORM\Column(name="maybeTo", type="text", nullable=true)
     */
    private $maybeTo;

    /**
     * @var string
     *
     * @ORM\Column(name="maybePrice", type="text", nullable=true)
     */
    private $maybePrice;

    /**
     * @var string
     *
     * @ORM\Column(name="maybeCurrency", type="text", nullable=true)
     */
    private $maybeCurrency;

    /**
     * @var string
     *
     * @ORM\Column(name="maybeUrl", type="text", nullable=true)
     */
    private $maybeUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="text")
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="maybePageFrom", type="text", nullable=true)
     */
    private $maybePageFrom;

    /**
     * @var string
     *
     * @ORM\Column(name="maybePageTo", type="text", nullable=true)
     */
    private $maybePageTo;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="created_at", type="datetime")
	 */
    private $createdAt;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="processed", type="boolean")
	 */
	private $processed = false;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="processed_end", type="boolean")
	 */
	private $processedEnd = false;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set site
     *
     * @param \AppBundle\Entity\Site $site
     *
     * @return MaybePost
     */
    public function setSite($site)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * Get site
     *
     * @return \AppBundle\Entity\Site
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Set maybeFrom
     *
     * @param string $maybeFrom
     *
     * @return MaybePost
     */
    public function setMaybeFrom($maybeFrom)
    {
        $this->maybeFrom = $maybeFrom;

        return $this;
    }

    /**
     * Get maybeFrom
     *
     * @return string
     */
    public function getMaybeFrom()
    {
        return $this->maybeFrom;
    }

    /**
     * Set maybeTo
     *
     * @param string $maybeTo
     *
     * @return MaybePost
     */
    public function setMaybeTo($maybeTo)
    {
        $this->maybeTo = $maybeTo;

        return $this;
    }

    /**
     * Get maybeTo
     *
     * @return string
     */
    public function getMaybeTo()
    {
        return $this->maybeTo;
    }

    /**
     * Set maybePrice
     *
     * @param string $maybePrice
     *
     * @return MaybePost
     */
    public function setMaybePrice($maybePrice)
    {
        $this->maybePrice = $maybePrice;

        return $this;
    }

    /**
     * Get maybePrice
     *
     * @return string
     */
    public function getMaybePrice()
    {
        return $this->maybePrice;
    }

    /**
     * Set maybeCurrency
     *
     * @param string $maybeCurrency
     *
     * @return MaybePost
     */
    public function setMaybeCurrency($maybeCurrency)
    {
        $this->maybeCurrency = $maybeCurrency;

        return $this;
    }

    /**
     * Get maybeCurrency
     *
     * @return string
     */
    public function getMaybeCurrency()
    {
        return $this->maybeCurrency;
    }

    /**
     * Set maybeUrl
     *
     * @param string $maybeUrl
     *
     * @return MaybePost
     */
    public function setMaybeUrl($maybeUrl)
    {
        $this->maybeUrl = $maybeUrl;

        return $this;
    }

    /**
     * Get maybeUrl
     *
     * @return string
     */
    public function getMaybeUrl()
    {
        return $this->maybeUrl;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return MaybePost
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set maybePageFrom
     *
     * @param string $maybePageFrom
     *
     * @return MaybePost
     */
    public function setMaybePageFrom($maybePageFrom)
    {
        $this->maybePageFrom = $maybePageFrom;

        return $this;
    }

    /**
     * Get maybePageFrom
     *
     * @return string
     */
    public function getMaybePageFrom()
    {
        return $this->maybePageFrom;
    }

    /**
     * Set maybePageTo
     *
     * @param string $maybePageTo
     *
     * @return MaybePost
     */
    public function setMaybePageTo($maybePageTo)
    {
        $this->maybePageTo = $maybePageTo;

        return $this;
    }

    /**
     * Get maybePageTo
     *
     * @return string
     */
    public function getMaybePageTo()
    {
        return $this->maybePageTo;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return MaybePost
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set processed
     *
     * @param boolean $processed
     *
     * @return MaybePost
     */
    public function setProcessed($processed)
    {
        $this->processed = $processed;

        return $this;
    }

    /**
     * Get processed
     *
     * @return boolean
     */
    public function getProcessed()
    {
        return $this->processed;
    }

    /**
     * Set processedEnd
     *
     * @param boolean $processedEnd
     *
     * @return MaybePost
     */
    public function setProcessedEnd($processedEnd)
    {
        $this->processedEnd = $processedEnd;

        return $this;
    }

    /**
     * Get processedEnd
     *
     * @return boolean
     */
    public function getProcessedEnd()
    {
        return $this->processedEnd;
    }
}
