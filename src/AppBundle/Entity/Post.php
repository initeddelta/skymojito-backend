<?php

namespace AppBundle\Entity;

use AppBundle\Library\CurrencyConvertor;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Post
 *
 * @ORM\Table(name="post")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PostRepository")
 */
class Post
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

	/**
	 * @var \AppBundle\Entity\MaybePost
	 *
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\MaybePost")
	 * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
	 */
    private $maybePost;

    /**
     * @var Destination
     *
     * @ORM\ManyToOne(targetEntity="Destination")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $destinationFrom;

    /**
     * @var Destination
     *
     * @ORM\ManyToOne(targetEntity="Destination")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $destinationTo;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float", nullable=true)
     */
    private $price;

	/**
	 * @var float
	 *
	 * @ORM\Column(name="price_in_eur", type="float", nullable=true)
	 */
    private $priceInEUR;

    /**
     * @var string
     *
     * @ORM\Column(name="currency", type="string", length=255, nullable=true)
     */
    private $currency;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="PostSite", mappedBy="post", cascade={"persist"})
     */
    private $postSites;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="url", type="string", length=255, nullable=true)
	 */
    private $url;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="inappropriate", type="boolean")
	 */
	private $inappropriate = false;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="inappropriate_reason", type="string", length=255, nullable=true)
	 */
	private $inappropriateReason;

	/**
	 * @var \Doctrine\Common\Collections\ArrayCollection
	 *
	 * @ORM\OneToMany(targetEntity="AppBundle\Entity\PostLocale", mappedBy="post", cascade={"persist", "remove"})
	 */
	private $postLocales;

	/**
	 * @param string $localeName
	 *
	 * @return string
	 */
	public function getTitle($localeName = 'cs'){

		$title = '';

		$from = $this->getDestinationFrom();
		$to = $this->getDestinationTo();
		$price = $this->getPrice();
		$currency = $this->getCurrency();

		switch($localeName){
			case 'cs' : {
				$sep = ' - ';
				$over = ' za ';
				$decimals = 0;
				$decPoint = ',';
				$thousands_sep = ' ';
			}break;
			default : {
				$sep = ' - ';
				$over = ' za ';
				$decimals = 0;
				$decPoint = ',';
				$thousands_sep = ' ';
			}break;
		}

		if($from){
			$title = $title.$from->getTitle($localeName);
		}
		if($from && $to){
			$title = $title.$sep;
		}
		if($to){
			$title = $title.$to->getTitle($localeName);
		}
		if($price && $currency){
			$title = $title.$over.(number_format($price, $decimals, $decPoint, $thousands_sep)).' '.$currency;
		}

		return $title;

	}

	/**
	 * @return bool
	 */
	public function isFullValid(){

		return !$this->getInappropriate() && $this->getDestinationTo() && $this->getDestinationFrom() && $this->getPrice() && $this->getCurrency();

	}

	public function getPerex($localeName = 'cs'){

		foreach($this->getPostLocales() as $postLocale){
			if($postLocale->getLocale()->getName() === $localeName){
				return $postLocale->getPerex();
			}
		}

		return null;

	}


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->postSites = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return Post
     */
    public function setPrice($price)
    {
        $this->price = $price;

        $rates = CurrencyConvertor::getRates('EUR');

        $this->priceInEUR = $this->price*$rates[$this->getCurrency()];

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

	/**
	 * @return float
	 */
	public function getPriceInEUR(){
		return $this->priceInEUR;
	}

    /**
     * Set currency
     *
     * @param string $currency
     *
     * @return Post
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Post
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Post
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set destinationFrom
     *
     * @param \AppBundle\Entity\Destination $destinationFrom
     *
     * @return Post
     */
    public function setDestinationFrom(\AppBundle\Entity\Destination $destinationFrom = null)
    {
        $this->destinationFrom = $destinationFrom;

        return $this;
    }

    /**
     * Get destinationFrom
     *
     * @return \AppBundle\Entity\Destination
     */
    public function getDestinationFrom()
    {
        return $this->destinationFrom;
    }

    /**
     * Set destinationTo
     *
     * @param \AppBundle\Entity\Destination $destinationTo
     *
     * @return Post
     */
    public function setDestinationTo(\AppBundle\Entity\Destination $destinationTo = null)
    {
        $this->destinationTo = $destinationTo;

        return $this;
    }

    /**
     * Get destinationTo
     *
     * @return \AppBundle\Entity\Destination
     */
    public function getDestinationTo()
    {
        return $this->destinationTo;
    }

    /**
     * Add postSite
     *
     * @param \AppBundle\Entity\PostSite $postSite
     *
     * @return Post
     */
    public function addPostSite(\AppBundle\Entity\PostSite $postSite)
    {
        $this->postSites[] = $postSite;

        return $this;
    }

    /**
     * Remove postSite
     *
     * @param \AppBundle\Entity\PostSite $postSite
     */
    public function removePostSite(\AppBundle\Entity\PostSite $postSite)
    {
        $this->postSites->removeElement($postSite);
    }

    /**
     * Get postSites
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPostSites()
    {
        return $this->postSites;
    }

    /**
     * Set maybePost
     *
     * @param \AppBundle\Entity\MaybePost $maybePost
     *
     * @return Post
     */
    public function setMaybePost(\AppBundle\Entity\MaybePost $maybePost = null)
    {
        $this->maybePost = $maybePost;

        return $this;
    }

    /**
     * Get maybePost
     *
     * @return \AppBundle\Entity\MaybePost
     */
    public function getMaybePost()
    {
        return $this->maybePost;
    }

    /**
     * Set inappropriate
     *
     * @param boolean $inappropriate
     *
     * @return Post
     */
    public function setInappropriate($inappropriate)
    {
        $this->inappropriate = $inappropriate;

        return $this;
    }

    /**
     * Get inappropriate
     *
     * @return boolean
     */
    public function getInappropriate()
    {
        return $this->inappropriate;
    }

    /**
     * Set inappropriateReason
     *
     * @param string $inappropriateReason
     *
     * @return Post
     */
    public function setInappropriateReason($inappropriateReason)
    {
        $this->inappropriateReason = $inappropriateReason;

        return $this;
    }

    /**
     * Get inappropriateReason
     *
     * @return string
     */
    public function getInappropriateReason()
    {
        return $this->inappropriateReason;
    }

    /**
     * Add postLocale
     *
     * @param \AppBundle\Entity\PostLocale $postLocale
     *
     * @return Post
     */
    public function addPostLocale(\AppBundle\Entity\PostLocale $postLocale)
    {
        $this->postLocales[] = $postLocale;
        $postLocale->setPost($this);

        return $this;
    }

    /**
     * Remove postLocale
     *
     * @param \AppBundle\Entity\PostLocale $postLocale
     */
    public function removePostLocale(\AppBundle\Entity\PostLocale $postLocale)
    {
        $this->postLocales->removeElement($postLocale);
    }

    /**
     * Get postLocales
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPostLocales()
    {
        return $this->postLocales;
    }
}
