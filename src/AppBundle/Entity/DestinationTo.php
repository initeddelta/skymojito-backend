<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DestinationFrom
 *
 * @ORM\Table(name="destination_to")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DestinationToRepository")
 */
class DestinationTo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

	/**
	 * @var \Doctrine\Common\Collections\ArrayCollection
	 *
	 * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Destination", mappedBy="destinationTos", cascade={"persist"})
	 */
	private $destinations;

	/**
	 * @var \AppBundle\Entity\Locale
	 *
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Locale")
	 * @ORM\JoinColumn(referencedColumnName="id")
	 */
	private $locale;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return DestinationFrom
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->destinations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add destination
     *
     * @param \AppBundle\Entity\Destination $destination
     *
     * @return DestinationFrom
     */
    public function addDestination(\AppBundle\Entity\Destination $destination)
    {
        $this->destinations[] = $destination;
//        $destination->addDestinationFrom($this);

        return $this;
    }

    /**
     * Remove destination
     *
     * @param \AppBundle\Entity\Destination $destination
     */
    public function removeDestination(\AppBundle\Entity\Destination $destination)
    {
        $this->destinations->removeElement($destination);
    }

    /**
     * Get destinations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDestinations()
    {
        return $this->destinations;
    }

    /**
     * Set locale
     *
     * @param \AppBundle\Entity\Locale $locale
     *
     * @return DestinationFrom
     */
    public function setLocale(\AppBundle\Entity\Locale $locale = null)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * Get locale
     *
     * @return \AppBundle\Entity\Locale
     */
    public function getLocale()
    {
        return $this->locale;
    }
}
