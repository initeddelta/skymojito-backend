<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DestinationLocale
 *
 * @ORM\Table(name="destination_locale")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DestinationLocaleRepository")
 */
class DestinationLocale
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="info", type="text", nullable=true)
	 */
	private $info;

    /**
     * @var \AppBundle\Entity\Destination
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Destination", inversedBy="destinationLocales", cascade={"persist", "remove"})
     */
    private $destination;

    /**
     * @var \AppBundle\Entity\Locale
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Locale", cascade={"persist", "remove"})
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $locale;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return DestinationLocale
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set destination
     *
     * @param \AppBundle\Entity\Destination $destination
     *
     * @return DestinationLocale
     */
    public function setDestination(\AppBundle\Entity\Destination $destination = null)
    {
        $this->destination = $destination;

        return $this;
    }

    /**
     * Get destination
     *
     * @return \AppBundle\Entity\Destination
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * Set locale
     *
     * @param \AppBundle\Entity\Locale $locale
     *
     * @return DestinationLocale
     */
    public function setLocale(\AppBundle\Entity\Locale $locale = null)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * Get locale
     *
     * @return \AppBundle\Entity\Locale
     */
    public function getLocale()
    {
        return $this->locale;
    }

	/**
	 * Set info
	 *
	 * @param string $info
	 *
	 * @return \AppBundle\Entity\DestinationLocale
	 */
	public function setInfo($info)
	{
		$this->info = $info;

		return $this;
	}

	/**
	 * Get info
	 *
	 * @return string
	 */
	public function getInfo()
	{
		return $this->info;
	}
}
