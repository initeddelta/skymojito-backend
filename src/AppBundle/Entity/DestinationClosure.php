<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Tree\Entity\MappedSuperclass\AbstractClosure;

/**
 * DestinationClosure
 *
 * @ORM\Table(name="destination_closure")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DestinationClosureRepository")
 */
class DestinationClosure extends AbstractClosure
{

}
