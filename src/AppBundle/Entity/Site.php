<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Site
 *
 * @ORM\Table(name="site")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SiteRepository")
 */
class Site
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255, unique=true)
     */
    private $code;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="title", type="string", length=255)
	 */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="uri", type="string", length=255, unique=true)
     */
    private $uri;

	/**
	 * @var string
	 * 
	 * @ORM\Column(name="path_to_medias", type="string", length=255)
	 */
	private $pathTo_Medias;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="path_to_destination_to", type="string", length=255)
	 */
	private $pathTo_DestinationTo;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="path_to_destination_from", type="string", length=255)
	 */
	private $pathTo_DestinationFrom;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="path_to_price", type="string", length=255)
	 */
	private $pathTo_Price;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="path_to_currency", type="string", length=255)
	 */
	private $pathTo_Currency;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="path_to_url", type="string", length=255)
	 */
	private $pathTo_URL;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="path_to_url_source", type="string", length=255)
	 */
	private $pathTo_URL_source;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="path_to_page_destination_to", type="string", length=255)
	 */
	private $pathTo_page_DestinationTo;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="path_to_page_destination_from", type="string", length=255)
	 */
	private $pathTo_page_DestinationFrom;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="PostSite", mappedBy="site", cascade={"persist"})
     */
    private $postSites;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="crawle_method", type="string", length=255)
	 */
    private $crawleMethod;

	/**
	 * @var \AppBundle\Entity\Locale
	 *
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Locale")
	 * @ORM\JoinColumn(referencedColumnName="id")
	 */
    private $locale;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Site
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set uri
     *
     * @param string $uri
     *
     * @return Site
     */
    public function setUri($uri)
    {
        $this->uri = $uri;

        return $this;
    }

    /**
     * Get uri
     *
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->postSites = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add postSite
     *
     * @param \AppBundle\Entity\PostSite $postSite
     *
     * @return Site
     */
    public function addPostSite(\AppBundle\Entity\PostSite $postSite)
    {
        $this->postSites[] = $postSite;

        return $this;
    }

    /**
     * Remove postSite
     *
     * @param \AppBundle\Entity\PostSite $postSite
     */
    public function removePostSite(\AppBundle\Entity\PostSite $postSite)
    {
        $this->postSites->removeElement($postSite);
    }

    /**
     * Get postSites
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPostSites()
    {
        return $this->postSites;
    }

    /**
     * Set pathToMedias
     *
     * @param string $pathToMedias
     *
     * @return Site
     */
    public function setPathToMedias($pathToMedias)
    {
        $this->pathTo_Medias = $pathToMedias;

        return $this;
    }

    /**
     * Get pathToMedias
     *
     * @return string
     */
    public function getPathToMedias()
    {
        return $this->pathTo_Medias;
    }

    /**
     * Set pathToDestinationTo
     *
     * @param string $pathToDestinationTo
     *
     * @return Site
     */
    public function setPathToDestinationTo($pathToDestinationTo)
    {
        $this->pathTo_DestinationTo = $pathToDestinationTo;

        return $this;
    }

    /**
     * Get pathToDestinationTo
     *
     * @return string
     */
    public function getPathToDestinationTo()
    {
        return $this->pathTo_DestinationTo;
    }

    /**
     * Set pathToDestinationFrom
     *
     * @param string $pathToDestinationFrom
     *
     * @return Site
     */
    public function setPathToDestinationFrom($pathToDestinationFrom)
    {
        $this->pathTo_DestinationFrom = $pathToDestinationFrom;

        return $this;
    }

    /**
     * Get pathToDestinationFrom
     *
     * @return string
     */
    public function getPathToDestinationFrom()
    {
        return $this->pathTo_DestinationFrom;
    }

    /**
     * Set pathToPrice
     *
     * @param string $pathToPrice
     *
     * @return Site
     */
    public function setPathToPrice($pathToPrice)
    {
        $this->pathTo_Price = $pathToPrice;

        return $this;
    }

    /**
     * Get pathToPrice
     *
     * @return string
     */
    public function getPathToPrice()
    {
        return $this->pathTo_Price;
    }

    /**
     * Set pathToCurrency
     *
     * @param string $pathToCurrency
     *
     * @return Site
     */
    public function setPathToCurrency($pathToCurrency)
    {
        $this->pathTo_Currency = $pathToCurrency;

        return $this;
    }

    /**
     * Get pathToCurrency
     *
     * @return string
     */
    public function getPathToCurrency()
    {
        return $this->pathTo_Currency;
    }

    /**
     * Set pathToURL
     *
     * @param string $pathToURL
     *
     * @return Site
     */
    public function setPathToURL($pathToURL)
    {
        $this->pathTo_URL = $pathToURL;

        return $this;
    }

    /**
     * Get pathToURL
     *
     * @return string
     */
    public function getPathToURL()
    {
        return $this->pathTo_URL;
    }

    /**
     * Set pathToPageDestinationTo
     *
     * @param string $pathToPageDestinationTo
     *
     * @return Site
     */
    public function setPathToPageDestinationTo($pathToPageDestinationTo)
    {
        $this->pathTo_page_DestinationTo = $pathToPageDestinationTo;

        return $this;
    }

    /**
     * Get pathToPageDestinationTo
     *
     * @return string
     */
    public function getPathToPageDestinationTo()
    {
        return $this->pathTo_page_DestinationTo;
    }

    /**
     * Set pathToPageDestinationFrom
     *
     * @param string $pathToPageDestinationFrom
     *
     * @return Site
     */
    public function setPathToPageDestinationFrom($pathToPageDestinationFrom)
    {
        $this->pathTo_page_DestinationFrom = $pathToPageDestinationFrom;

        return $this;
    }

    /**
     * Get pathToPageDestinationFrom
     *
     * @return string
     */
    public function getPathToPageDestinationFrom()
    {
        return $this->pathTo_page_DestinationFrom;
    }

    /**
     * Set pathToURLSource
     *
     * @param string $pathToURLSource
     *
     * @return Site
     */
    public function setPathToURLSource($pathToURLSource)
    {
        $this->pathTo_URL_source = $pathToURLSource;

        return $this;
    }

    /**
     * Get pathToURLSource
     *
     * @return string
     */
    public function getPathToURLSource()
    {
        return $this->pathTo_URL_source;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Site
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set crawleMethod
     *
     * @param string $crawleMethod
     *
     * @return Site
     */
    public function setCrawleMethod($crawleMethod)
    {
        $this->crawleMethod = $crawleMethod;

        return $this;
    }

    /**
     * Get crawleMethod
     *
     * @return string
     */
    public function getCrawleMethod()
    {
        return $this->crawleMethod;
    }

    /**
     * Set locale
     *
     * @param \AppBundle\Entity\Locale $locale
     *
     * @return Site
     */
    public function setLocale(\AppBundle\Entity\Locale $locale = null)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * Get locale
     *
     * @return \AppBundle\Entity\Locale
     */
    public function getLocale()
    {
        return $this->locale;
    }
}
