<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PostLocale
 *
 * @ORM\Table(name="post_locale")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PostLocaleRepository")
 */
class PostLocale
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

	/**
	 * @var \AppBundle\Entity\Destination
	 *
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Post", inversedBy="postLocales", cascade={"persist", "remove"})
	 */
    private $post;

	/**
	 * @var \AppBundle\Entity\Locale
	 *
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Locale", cascade={"persist", "remove"})
	 * @ORM\JoinColumn(referencedColumnName="id")
	 */
    private $locale;

    /**
     * @var string
     *
     * @ORM\Column(name="perex", type="text", nullable=true)
     */
    private $perex;




    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set perex
     *
     * @param string $perex
     *
     * @return PostLocale
     */
    public function setPerex($perex)
    {
        $this->perex = $perex;

        return $this;
    }

    /**
     * Get perex
     *
     * @return string
     */
    public function getPerex()
    {
        return $this->perex;
    }

    /**
     * Set post
     *
     * @param \AppBundle\Entity\Post $post
     *
     * @return PostLocale
     */
    public function setPost(\AppBundle\Entity\Post $post = null)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get post
     *
     * @return \AppBundle\Entity\Post
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * Set locale
     *
     * @param \AppBundle\Entity\Locale $locale
     *
     * @return PostLocale
     */
    public function setLocale(\AppBundle\Entity\Locale $locale = null)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * Get locale
     *
     * @return \AppBundle\Entity\Locale
     */
    public function getLocale()
    {
        return $this->locale;
    }
}
