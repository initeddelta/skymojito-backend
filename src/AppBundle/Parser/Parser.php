<?php
	/**
	 * Project: fly-tickets-aggregator
	 * File: AbstractParser.php
	 * Author: Tomas SYROVY <tomas@syrovy.pro>
	 * Date: 07.08.17
	 * Version: 1.0
	 */

	namespace AppBundle\Parser;


	use AppBundle\Entity\Destination;
	use AppBundle\Entity\MaybePost;
	use AppBundle\Entity\Post;
    use AppBundle\Library\CurrencyConvertor;
    use AppBundle\Library\Library;
	use Doctrine\Common\Persistence\ObjectManager;

	class Parser {

		/**
		 * @var \Doctrine\ORM\EntityManagerInterface
		 */
		private $manager;

		/**
		 * @var \AppBundle\Entity\MaybePost
		 */
		private $maybePost;

		/**
		 * @var boolean
		 */
		private $inappropriate = false;

		/**
		 * @var string
		 */
		private $inappropriateReason;

		/**
		 * Parser constructor.
		 *
		 * @param \Doctrine\Common\Persistence\ObjectManager $manager
		 * @param \AppBundle\Entity\MaybePost          $maybePost
		 */
		public function __construct( ObjectManager $manager, MaybePost $maybePost ){
			$this->manager   = $manager;
			$this->maybePost = $maybePost;
		}

		private function getBadWords(){

			return Library::$badWords;

		}

		/**
		 * @param bool $debug
		 *
		 * @return \AppBundle\Entity\Post
		 */
		public function createAndGetPost($debug = false){

			if($debug){
				echo '= Kandidát na příspěvek';
				echo '<br>';
				echo '== id: '.$this->maybePost->getId();
				echo '<br>';
				echo '== url: '.$this->maybePost->getUrl();
				echo '<br>';
				echo '== maybeFrom: '.$this->maybePost->getMaybeFrom();
				echo '<br>';
				echo '== maybeTo: '.$this->maybePost->getMaybeTo();
				echo '<br>';
				echo '== maybePrice: '.$this->maybePost->getMaybePrice();
				echo '<br>';
				echo '== maybeCurrency: '.$this->maybePost->getMaybeCurrency();
				echo '<br>';
				echo '<br>';
			}


			$post = new Post();
			$post->setCreatedAt(new \DateTime());
			$post->setUrl($this->maybePost->getUrl());
			$post->setMaybePost($this->maybePost);

			if($debug){
				echo '= Příspěvek';
				echo '<br>';
			}

			$from = $this->getDestinationFrom($this->maybePost->getMaybeFrom(), $this->maybePost->getMaybePageFrom(), $debug);
			$to = $this->getDestinationTo($this->maybePost->getMaybeTo(), $this->maybePost->getMaybePageTo(), $debug);
			$price = $this->getPrice($this->maybePost->getMaybePrice(), $debug);
			$currency = $this->getCurrency($this->maybePost->getMaybeCurrency(), $debug);

			if($from === $to){
				$this->inappropriate = true;
				$this->inappropriateReason = 'Same TO and FROM';
			}

			if($from !== null && $to !== null){

				$ft = $from->getLevel();
				$tt = $to->getLevel();

				if($ft !== $tt){

					/** @var \Gedmo\Tree\Entity\Repository\ClosureTreeRepository $repository */
					$repository = $this->manager->getRepository(Destination::class);

					if($ft > $tt){
						$children = $repository->getChildren($to);
						if(in_array($from, $children, true)){
							$this->inappropriate = true;
							$this->inappropriateReason = 'FROM and TO in parents rels.';
							$to = null;
						}
					}
					if($ft < $tt){
						$children = $repository->getChildren($from);
						if(in_array($to, $children, true)){
							$this->inappropriate = true;
							$this->inappropriateReason = 'FROM and TO in parents rels.';
							$from = null;
						}
					}

				}

			}

			$post->setDestinationFrom($from);
			$post->setDestinationTo($to);
            $post->setCurrency($currency);
			$post->setPrice($price);
			$post->setInappropriate($this->inappropriate);
			$post->setInappropriateReason($this->inappropriateReason);

			return $post;

		}

		/**
		 * @param $string
		 *
		 * @return string
		 */
		private function cleanString($string){

			$string = preg_replace("/&#?[a-z0-9]{2,8};/i","", $string);

			$badChars = [
				'.',
				',',
				'?',
				'!',
				'/',
				'-',
				':',
				';',
				')',
				'(',
				']',
				'[',
				'}',
				'{',
			];

			return str_replace($badChars, '' , trim(mb_strtolower($string)));

		}

		private function getDestinationTo($maybeTo, $maybeTo_page, $debug){

			$maybeTo = $this->cleanString($maybeTo);
			$maybeTo_page = $this->cleanString($maybeTo_page);
//			$maybeTo_page = '';

			$words = explode(' ', $maybeTo.' '.$maybeTo_page);
			$strings = [];
			foreach($words as $key => $word){
				if(strlen($word) >= 3){
					$word = str_replace('"', '', $word);
					$strings[] = '('.$key.', "'.$word.'")';
				}
			}

			if($debug){
				echo '== TO';
				echo '<br>';
				echo '=== strings: '.implode(', ', $strings);
				echo '<br>';
			}

			$query = 'CREATE TEMPORARY TABLE IF NOT EXISTS word (`index` INT, title VARCHAR(100)) ENGINE=InnoDB DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci;';
			$result = $this->manager->getConnection()->executeQuery($query)->execute();

			$query = '
				INSERT INTO word (`index`, title) VALUES '.implode(',', $strings).';';
			$result = $this->manager->getConnection()->executeQuery($query)->execute();

			$query = '
				SELECT d_id, d_title, d_priority, d_alternative, d_type, w.title as w_title, w.`index` as w_index, jaro_winkler_similarity(d_title, w.title) as similarity FROM (SELECT d.id as d_id, dl.title as d_title, d.priority as d_priority, d.type AS d_type, d.alternative AS d_alternative FROM destination d JOIN destination_locale dl ON d.id = dl.destination_id JOIN locale l ON dl.locale_id = l.id WHERE l.name = "'.$this->maybePost->getSite()->getLocale()->getName().'") d, word w WHERE SUBSTRING(d_title, 1, 3) = SUBSTRING(w.title, 1, 3) AND jaro_winkler_similarity(d_title, w.title) > 0.9 AND d_priority IN (1, 2) AND ((d_alternative IN (0) AND d_type IN ("04-country", "05-city")) OR (d_alternative IN (1)));
			';

			$result = $this->manager->getConnection()->executeQuery($query)->fetchAll();

			$query = 'DROP TEMPORARY TABLE IF EXISTS word;';
			$resultE = $this->manager->getConnection()->executeQuery($query)->execute();

			if($debug){
				echo '==== SIMILARS:';
				echo '<br>';
			}

			$cleanResult = [];
			foreach($result as $row){

				if($row['d_alternative'] == 1){
					$a = $this->manager->getRepository(Destination::class)->find($row['d_id']);
					if($a && $a->getAlternativeParent()){
						$a = $this->manager->getRepository(Destination::class)->find($a->getAlternativeParent());
						$row['d_id'] = $a->getId();
						$row['d_title'] = $a->getTitle($this->maybePost->getSite()->getLocale()->getName());
						$row['d_priority'] = $a->getPriority();
						$row['d_type'] = $a->getType();
					}
				}

//				similar_text($row['d_title'], $row['w_title'], $percent);
//				$levenshtein = levenshtein($row['d_title'], $row['w_title']);
//				$stringCompare = self::string_compare($row['d_title'], $row['w_title']);
//
//				//limity podobnosti, které musí být proraženy
//				if($percent <= 70 or $levenshtein >= 3 or $stringCompare <= 0.6){
//					continue;
//				}
//
//				$s = 0.05*(($percent/100)*$stringCompare)*100-3;

				$s = 0.05*$row['similarity']*100-3;

				$k = 1;
				switch($this->maybePost->getSite()->getLocale()->getName()){
					case 'cs' : {
						if(array_key_exists($row['w_index']-1, $words)){
							if(in_array($words[$row['w_index']-1], ['do', 'na'], false)){
								$k = 2;
							}
							if(in_array($words[$row['w_index']-1], ['z'], false)){
								$k = 0;
							}
						}
					}break;
					case 'sk' : {
						if(array_key_exists($row['w_index']-1, $words)){
							if(in_array($words[$row['w_index']-1], ['do', 'na'], false)){
								$k = 2;
							}
							if(in_array($words[$row['w_index']-1], ['z'], false)){
								$k = 0;
							}
						}
					}break;
					case 'pl' : {
						if(array_key_exists($row['w_index']-1, $words)){
							if(in_array($words[$row['w_index']-1], ['do', 'na'], false)){
								$k = 2;
							}
							if(in_array($words[$row['w_index']-1], ['z'], false)){
								$k = 0;
							}
						}
					}break;
					case 'en' : {
						if(array_key_exists($row['w_index']-1, $words)){
							if(in_array($words[$row['w_index']-1], ['to', 'in', 'on'], false)){
								$k = 2;
							}
							if(in_array($words[$row['w_index']-1], ['from'], false)){
								$k = 0;
							}
						}
					}break;
					case 'de' : {
						if(array_key_exists($row['w_index']-1, $words)){
							if(in_array($words[$row['w_index']-1], ['auch', 'auf'], false)){
								$k = 2;
							}
							if(in_array($words[$row['w_index']-1], ['aus'], false)){
								$k = 0;
							}
						}
					}break;
					case 'ru' : {
						if(array_key_exists($row['w_index']-1, $words)){
							if(in_array($words[$row['w_index']-1], ['в', 'до', 'на'], false)){
								$k = 2;
							}
							if(in_array($words[$row['w_index']-1], ['из'], false)){
								$k = 0;
							}
						}
					}break;
				}

				switch($row['d_priority']){
					case 1 : {
						$p = 4;
					}break;
					case 2 : {
						$p = 6;
					}break;
					case 3 : {
						$p = 14;
					}break;
					default : {
						$p = 14;
					}break;
				}

				$titleIndex = ($row['w_index']*1+1) <= substr_count( $maybeTo, ' ' )+1 ? 5 : 1;

				if(array_key_exists($row['w_index']-1, $words)){
					if(in_array(strtolower($words[$row['w_index']-1]), $this->getBadWords(), false)){
						continue;
					}
				}
				if(array_key_exists($row['w_index']+1, $words)){
					if(in_array(strtolower($words[$row['w_index']+1]), $this->getBadWords(), false)){
						continue;
					}
				}

				$d = $titleIndex*$s*$k/$p;
				if($d <= 0.3){
					continue;
				}

				$cleanResult[] = [
					'w_title' => $row['w_title'],
					'w_index' => $row['w_index'],
					'd_title' => $row['d_title'],
					'd_id' => $row['d_id'],
					'similarity' => $row['similarity'],
//					'similarText' => $percent,
//					'levenstein' => $levenshtein,
//					'stringCompare' => $stringCompare,
					's' => $s,
					'k' => $k,
					'p' => $p,
					't' => $titleIndex,
					'd' => $d
				];

				if($debug){
					echo '=====: '.$row['w_title'].' -> '.$row['d_title'];
					echo '<br>';
					echo '======: similarity: '.$row['similarity'];
					echo '<br>';
					echo '======: s: '.$s;
					echo '<br>';
					echo '======: k: '.$k;
					echo '<br>';
					echo '======: p: '.$p;
					echo '<br>';
					echo '======: t: '.$titleIndex;
					echo '<br>';
					echo '======: d: '.$titleIndex*$s*$k/$p;
					echo '<br>';
				}

			}

			//Hodnota "d" pro stejné destinace se sčítají
			$middleResult = [];
			foreach($cleanResult as $r){
				if(array_key_exists($r['d_id'], $middleResult)){
					$middleResult[$r['d_id']]['d'] = $middleResult[$r['d_id']]['d'] + $r['d'];
				}else{
					$middleResult[$r['d_id']] = $r;
				}
			}

			//Potomci získavají hodnotu "d" ze svých rodičů
			foreach($middleResult as $r){
				/** @var Destination $destinationEntity */
				$destinationEntity = $this->manager->getRepository(Destination::class)->find($r['d_id']);
				/** @var Destination $destinationEntityParent */
				$destinationEntityParent = $destinationEntity->getParent();
				if($destinationEntityParent){
					if(array_key_exists($destinationEntityParent->getId(), $middleResult)){
						$middleResult[$destinationEntity->getId()]['d'] = $middleResult[$destinationEntity->getId()]['d'] + $middleResult[$destinationEntityParent->getId()]['d'];
					}
				}
			}


//			if($destinationEntityParent){
//				if(array_key_exists($destinationEntityParent->getId(), $middleResult)){
//					dump($middleResult[$destinationEntityParent->getId()]['d']);
//					$middleResult[$r['d_id']]['d'] = $middleResult[$r['d_id']]['d'] + $middleResult[$destinationEntityParent->getId()]['d'];
//				}
//			}

			uasort($middleResult, function($a, $b){
				return $a['d'] < $b['d'];
			});

			$keys = array_keys($middleResult);

//			if(array_key_exists(0, $keys) and array_key_exists(1, $keys)){
//				$d1 = $middleResult[$keys[0]];
//				$d2 = $middleResult[$keys[1]];
//				if(min([$d1['d'], $d2['d']])/max([$d1['d'], $d2['d']]) >= 0.9){
//					$this->inappropriate = true;
//					$destination = $this->manager->getRepository(Destination::class)->find($keys[0]);
//					$this->inappropriateReason = 'TOs are in 10% limits. ['.$destination->getTitle().']';
//				}
//			}

			if(array_key_exists(0, $keys)){
				$destination = $this->manager->getRepository(Destination::class)->find($keys[0]);
				if($destination->getAlternative()){
					if($debug){
						echo '======: RESULT: '.$destination->getAlternativeParent()->getTitle();
						echo '<br>';
					}
					return $destination->getAlternativeParent();
				}
				if($debug){
					echo '======: RESULT: '.$destination->getTitle();
					echo '<br>';
				}
				return $destination;
			}

			if($debug){
				echo '======: RESULT: NULL';
				echo '<br>';
			}
			return null;

		}

		private function getDestinationFrom($maybeFrom, $maybeFrom_page, $debug){

			$maybeFrom = $this->cleanString($maybeFrom);
			$maybeFrom_page = $this->cleanString($maybeFrom_page);

			if(!$maybeFrom){
				return null;
			}

			$words = explode(' ', $maybeFrom.' '.$maybeFrom_page);
			$strings = [];
			foreach($words as $key => $word){
				if(strlen($word) >= 3){
					$word = str_replace('"', '', $word);
					$strings[] = '('.$key.', "'.$word.'")';
				}
			}

			if($debug){
				echo '== FROM';
				echo '<br>';
				echo '=== strings: '.implode(', ', $strings);
				echo '<br>';
			}

			$query = 'CREATE TEMPORARY TABLE IF NOT EXISTS word (`index` INT, title VARCHAR(100)) ENGINE=InnoDB DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci;';
			$result = $this->manager->getConnection()->executeQuery($query)->execute();

			$query = '
				INSERT INTO word (`index`, title) VALUES '.implode(',', $strings).';';
			$result = $this->manager->getConnection()->executeQuery($query)->execute();

			$query = '
				SELECT d_id, d_title, d_priority, d_alternative, d_type, w.title as w_title, w.`index` as w_index, jaro_winkler_similarity(d_title, w.title) as similarity FROM (SELECT d.id as d_id, dl.title as d_title, d.priority as d_priority, d.type AS d_type, d.alternative AS d_alternative FROM destination d JOIN destination_locale dl ON d.id = dl.destination_id JOIN locale l ON dl.locale_id = l.id WHERE l.name = "'.$this->maybePost->getSite()->getLocale()->getName().'") d, word w WHERE SUBSTRING(d_title, 1, 3) = SUBSTRING(w.title, 1, 3) AND jaro_winkler_similarity(d_title, w.title) > 0.9 AND d_priority IN (1, 2) AND ((d_alternative IN (0) AND d_type IN ("04-country", "05-city")) OR (d_alternative IN (1)));
			';

			$result = $this->manager->getConnection()->executeQuery($query)->fetchAll();

			$query = 'DROP TEMPORARY TABLE IF EXISTS word;';
			$resultE = $this->manager->getConnection()->executeQuery($query)->execute();

			if($debug){
				echo '==== SIMILARS:';
				echo '<br>';
			}

			$cleanResult = [];
			foreach($result as $row){

				if($row['d_alternative'] == 1){
					$a = $this->manager->getRepository(Destination::class)->find($row['d_id']);
					if($a && $a->getAlternativeParent()){
						$a = $this->manager->getRepository(Destination::class)->find($a->getAlternativeParent());
						$row['d_id'] = $a->getId();
						$row['d_title'] = $a->getTitle($this->maybePost->getSite()->getLocale()->getName());
						$row['d_priority'] = $a->getPriority();
						$row['d_type'] = $a->getType();
					}
				}

//				similar_text($row['d_title'], $row['w_title'], $percent);
//				$levenshtein = levenshtein($row['d_title'], $row['w_title']);
//				$stringCompare = self::string_compare($row['d_title'], $row['w_title']);
//
//				//limity podobnosti, které musí být proraženy
//				if($percent <= 70 or $levenshtein >= 3 or $stringCompare <= 0.6){
//					continue;
//				}
//
//				$s = 0.05*(($percent/100)*$stringCompare)*100-3;

				$s = 0.03*$row['similarity']*100-1;

				$k = 0; //pokud před slovem není nic, tak k = 0
				switch($this->maybePost->getSite()->getLocale()->getName()){
					case 'cs' : {
						if(array_key_exists($row['w_index']-1, $words)){
							if(in_array($words[$row['w_index']-1], ['do', 'na'], false)){
								$k = 0;
							}
							if(in_array($words[$row['w_index']-1], ['z'], false)){
								$k = 2; //pokud je tam z, tak k = 2
							}
						}
					}break;
					case 'sk' : {
						if(array_key_exists($row['w_index']-1, $words)){
							if(in_array($words[$row['w_index']-1], ['do', 'na'], false)){
								$k = 0;
							}
							if(in_array($words[$row['w_index']-1], ['z'], false)){
								$k = 2; //pokud je tam z, tak k = 2
							}
						}
					}break;
					case 'pl' : {
						if(array_key_exists($row['w_index']-1, $words)){
							if(in_array($words[$row['w_index']-1], ['do', 'na'], false)){
								$k = 0;
							}
							if(in_array($words[$row['w_index']-1], ['z'], false)){
								$k = 2; //pokud je tam z, tak k = 2
							}
						}
					}break;
					case 'en' : {
						if(array_key_exists($row['w_index']-1, $words)){
							if(in_array($words[$row['w_index']-1], ['to', 'in', 'on'], false)){
								$k = 0;
							}
							if(in_array($words[$row['w_index']-1], ['from'], false)){
								$k = 2; //pokud je tam z, tak k = 2
							}
						}
					}break;
					case 'de' : {
						if(array_key_exists($row['w_index']-1, $words)){
							if(in_array($words[$row['w_index']-1], ['auf', 'nach'], false)){
								$k = 0;
							}
							if(in_array($words[$row['w_index']-1], ['aus'], false)){
								$k = 2; //pokud je tam z, tak k = 2
							}
						}
					}break;
					case 'ru' : {
						if(array_key_exists($row['w_index']-1, $words)){
							if(in_array($words[$row['w_index']-1], ['в', 'до', 'на'], false)){
								$k = 0;
							}
							if(in_array($words[$row['w_index']-1], ['из'], false)){
								$k = 2; //pokud je tam z, tak k = 2
							}
						}
					}break;
				}

				switch($row['d_priority']){
					case 1 : {
						$p = 3;
					}break;
					case 2 : {
						$p = 4;
					}break;
					case 3 : {
						$p = 12;
					}break;
					default : {
						$p = 12;
					}break;
				}

				$titleIndex = ($row['w_index']*1+1) <= substr_count( $maybeFrom, ' ' )+1 ? 2 : 1;

				if(array_key_exists($row['w_index']-1, $words)){
					if(in_array(strtolower($words[$row['w_index']-1]), $this->getBadWords(), false)){
						continue;
					}
				}
				if(array_key_exists($row['w_index']+1, $words)){
					if(in_array(strtolower($words[$row['w_index']+1]), $this->getBadWords(), false)){
						continue;
					}
				}

				$d = $titleIndex*$s*$k/$p;
				if($d <= 0.3){
					continue;
				}

				$cleanResult[] = [
					'w_title' => $row['w_title'],
					'w_index' => $row['w_index'],
					'd_title' => $row['d_title'],
					'd_id' => $row['d_id'],
					'similarity' => $row['similarity'],
//					'similarText' => $percent,
//					'levenstein' => $levenshtein,
//					'stringCompare' => $stringCompare,
					's' => $s,
					'k' => $k,
					'p' => $p,
					't' => $titleIndex,
					'd' => $d,
				];

				if($debug){
					echo '=====: '.$row['w_title'].' -> '.$row['d_title'];
					echo '<br>';
					echo '======: similarity: '.$row['similarity'];
					echo '<br>';
					echo '======: s: '.$s;
					echo '<br>';
					echo '======: k: '.$k;
					echo '<br>';
					echo '======: p: '.$p;
					echo '<br>';
					echo '======: t: '.$titleIndex;
					echo '<br>';
					echo '======: d: '.$titleIndex*$s*$k/$p;
					echo '<br>';
				}

			}

			//Hodnota "d" pro stejné destinace se sčítají
			$middleResult = [];
			foreach($cleanResult as $r){
				if(array_key_exists($r['d_id'], $middleResult)){
					$middleResult[$r['d_id']]['d'] = $middleResult[$r['d_id']]['d'] + $r['d'];
				}else{
					$middleResult[$r['d_id']] = $r;
				}
			}

			//Potomci získavají hodnotu "d" ze svých rodičů
			foreach($middleResult as $r){
				/** @var Destination $destinationEntity */
				$destinationEntity = $this->manager->getRepository(Destination::class)->find($r['d_id']);
				/** @var Destination $destinationEntityParent */
				$destinationEntityParent = $destinationEntity->getParent();
				if($destinationEntityParent){
					if(array_key_exists($destinationEntityParent->getId(), $middleResult)){
						$middleResult[$destinationEntity->getId()]['d'] = $middleResult[$destinationEntity->getId()]['d'] + $middleResult[$destinationEntityParent->getId()]['d'];
					}
				}
			}

			uasort($middleResult, function($a, $b){
				return $a['d'] < $b['d'];
			});

			$keys = array_keys($middleResult);

			if(array_key_exists(0, $keys) and array_key_exists(1, $keys)){
				$d1 = $middleResult[$keys[0]];
				$d2 = $middleResult[$keys[1]];
				if(min([$d1['d'], $d2['d']])/max([$d1['d'], $d2['d'], 0.000001]) >= 0.8){
					$this->inappropriate = true;
					$destination = $this->manager->getRepository(Destination::class)->find($keys[0]);
					$this->inappropriateReason = 'FROMs are in 20% limits. ['.$destination->getTitle().']';

					if($debug){
						echo '======: RESULT: '.$this->inappropriateReason;
						echo '<br>';
					}

					return null;
				}
			}

			if(array_key_exists(0, $keys)){
				$destination = $this->manager->getRepository(Destination::class)->find($keys[0]);
				if($destination->getAlternative()){
					if($debug){
						echo '======: RESULT: '.$destination->getAlternativeParent()->getTitle();
						echo '<br>';
					}
					return $destination->getAlternativeParent();
				}
				if($debug){
					echo '======: RESULT: '.$destination->getTitle(null);
					echo '<br>';
				}
				return $destination;
			}

			if($debug){
				echo '======: RESULT: NULL';
				echo '<br>';
			}
			return null;

		}

//		private function getDestinationTo($maybeTo, $maybeTo_page){
//
//			$maybeTo = $this->cleanString($maybeTo);
//			$maybeTo_page = $this->cleanString($maybeTo_page);
//
//			$criteria = [];
//			$orderBy = [];
//			$destinations = $this->manager->getRepository(Destination::class)->findBy($criteria, $orderBy);
//
//			$words = explode(' ', $maybeTo.' '.$maybeTo_page);
//
//			$result = [];
//
//			$i = 1;
//			foreach($words as $key => $word){
//
//				$titleIndex = 1;
//				if($i <= str_word_count($maybeTo)){
//					$titleIndex = 3;
//				}
//
//				$cleanWord = substr($word, 0, -1);
//
//				if(strlen($cleanWord) <= 1){
//					continue;
//				}
//
//				foreach($destinations as $destination){
//
//					$cleanDestination = substr($destination->getTitle(), 0, -1);
//
//					similar_text($cleanWord, $cleanDestination, $percent);
//					$levenshtein = levenshtein($cleanWord, $cleanDestination);
//					$stringCompare = self::string_compare($cleanWord, $cleanDestination);
//
//					//limity podobnosti, které musí být proraženy
//					if($percent <= 70 or $levenshtein >= 3 or $stringCompare <= 0.6){
//						continue;
//					}
//
//					$s = 0.05*(($percent/100)*$stringCompare)*100-3;
//
//					$k = 1;
//					if(array_key_exists($key-1, $words)){
//						if(in_array($words[$key-1], ['do', 'na'], false)){
//							$k = 2;
//						}
//						if(in_array($words[$key-1], ['z'], false)){
//							$k = 0;
//						}
//					}
//
//					switch($destination->getPriority()){
//						case 1 : {
//							$p = 4;
//						}break;
//						case 2 : {
//							$p = 6;
//						}break;
//						case 3 : {
//							$p = 14;
//						}break;
//						default : {
//							$p = 14;
//						}break;
//					}
//
//					$result[$cleanWord][] = [
//						'word' => $word,
//						'destination' => $destination,
//						'similarText' => $percent,
//						'levenstein' => $levenshtein,
//						'stringCompare' => $stringCompare,
//						's' => $s,
//						'k' => $k,
//						'p' => $p,
//						't' => $titleIndex,
//						'c' => $titleIndex*$s*$k,
//						'd' => $titleIndex*$s*$k/$p
//					];
//
//				}
//
//				$i++;
//
//			}
//
//			//Ke každému slovu řadíme kandidáty podle d
//			$r = [];
//			foreach($result as $ke => $resultCleanWord){
//
//				usort($resultCleanWord, function($a, $b){
//					return $a['d'] < $b['d'];
//				});
//
//				$r[$ke] = $resultCleanWord;
//
//			}
//			$result = $r;
//
//			//Ke každému slovu přiřadíme nejlepšího kandidádat dle d
//			$r = [];
//			foreach($result as $ke => $resultCleanWord){
//
//				if(array_key_exists(0, $resultCleanWord)){
////					if($resultCleanWord[0]['d'] >= 0){
//						$r[$ke] = $resultCleanWord[0];
////					}
//				}
//
//			}
//			$result = $r;
//
////			return $result;
//
//			$r = [];
//			foreach($result as $ke => $resultCleanWord){
//				$r[$resultCleanWord['d']] = $resultCleanWord['destination'];
//			}
//			$result = $r;
//
//			return $result[max(array_keys($result))];
//
//		}

//		private function getDestinationFrom($maybeFrom, $maybeFrom_page){
//
//			$maybeFrom = $this->cleanString($maybeFrom);
//			$maybeFrom_page = $this->cleanString($maybeFrom_page);
//
//			$criteria = [];
//			$orderBy = [];
//			$destinations = $this->manager->getRepository(Destination::class)->findBy($criteria, $orderBy);
//
//			$words = explode(' ', $maybeFrom.' '.$maybeFrom_page);
//
//			$result = [];
//
//			$i = 1;
//			foreach($words as $key => $word){
//
//				$titleIndex = 1;
//				if($i <= str_word_count($maybeFrom)){
//					$titleIndex = 2;
//				}
//
//				$cleanWord = substr($word, 0, -1);
//
//				if(strlen($cleanWord) <= 1){
//					continue;
//				}
//
//				foreach($destinations as $destination){
//
//					$cleanDestination = substr($destination->getTitle(), 0, -1);
//
//					similar_text($cleanWord, $cleanDestination, $percent);
//					$levenshtein = levenshtein($cleanWord, $cleanDestination);
//					$stringCompare = self::string_compare($cleanWord, $cleanDestination);
//
//					//limity podobnosti, které musí být proraženy
//					if($percent <= 70 or $levenshtein >= 3 or $stringCompare <= 0.6){
//						continue;
//					}
//
//					$s = 0.03*(($percent/100)*$stringCompare)*100-1;
//
//					$k = 0.5;
//					if(array_key_exists($key-1, $words)){
//						if(in_array($words[$key-1], ['do', 'na', 'Do', 'Na'], false)){
//							$k = 0;
//						}
//						if(in_array($words[$key-1], ['z', 'Z'], false)){
//							$k = 2;
//						}
//					}
//
//					switch($destination->getPriority()){
//						case 1 : {
//							$p = 3;
//						}break;
//						case 2 : {
//							$p = 4;
//						}break;
//						case 3 : {
//							$p = 12;
//						}break;
//						default : {
//							$p = 12;
//						}break;
//					}
//
//					$result[$cleanWord][] = [
//						'word' => $word,
//						'destination' => $destination,
//						'similarText' => $percent,
//						'levenstein' => $levenshtein,
//						'stringCompare' => $stringCompare,
//						's' => $s,
//						'k' => $k,
//						'p' => $p,
//						't' => $titleIndex,
//						'c' => $titleIndex*$s*$k,
//						'd' => $titleIndex*$s*$k/$p
//					];
//
//				}
//
//				$i++;
//
//			}
//
//			//Ke každému slovu řadíme kandidáty podle d
//			$r = [];
//			foreach($result as $ke => $resultCleanWord){
//
//				usort($resultCleanWord, function($a, $b){
//					return $a['d'] < $b['d'];
//				});
//
//				$r[$ke] = $resultCleanWord;
//
//			}
//			$result = $r;
//
//			//Ke každému slovu přiřadíme nejlepšího kandidádat dle d
//			$r = [];
//			foreach($result as $ke => $resultCleanWord){
//
//				if(array_key_exists(0, $resultCleanWord)){
////					if($resultCleanWord[0]['d'] >= 0){
//					$r[$ke] = $resultCleanWord[0];
////					}
//				}
//
//			}
//			$result = $r;
//
////			return $result;
//
//			$r = [];
//			foreach($result as $ke => $resultCleanWord){
//				$r[$resultCleanWord['d']] = $resultCleanWord['destination'];
//			}
//			$result = $r;
//
//			return $result[max(array_keys($result))];
//
//		}

		/**
		 * @param $maybePrice
		 *
		 * @return mixed
		 */
		private function getPrice($maybePrice, $debug){

			$maybePrice = $this->cleanString($maybePrice);

			$str = preg_replace("/\s| |&nbsp;/",'', $maybePrice);
			preg_match_all("/[0-9]+[  .]{0,1}[0-9]*/", $str, $output_array);

			$numbers = [0];

			if(array_key_exists(0, $output_array)){
				foreach($output_array[0] as $item){

					$numbers[] = (int) preg_replace("/[  .]?/", "", $item);

				}
			}

			$numbers[] = max($numbers);
			$c = array_count_values($numbers);
			if(count($c) === 0){
				$this->inappropriate = true;
				$this->inappropriateReason = 'No price.';
				if($debug){
					echo '====: price: '.$this->inappropriateReason;
					echo '<br>';
				}
				return null;
			}

			if($debug){
				echo '====: price: '.array_search(max($c), $c);
				echo '<br>';
			}

			return array_search(max($c), $c);

		}

		private function getCurrency($maybeCurrency, $debug){

			$maybeCurrency = $this->cleanString($maybeCurrency);

			preg_match_all("/Kč|CZK|€|EUR\b|PLN|£|GBP|RUB\b/ui", $maybeCurrency, $output_array); //TODO - měny

			$currencies = [];

			if(array_key_exists(0, $output_array)){

				foreach($output_array[0] as $currency){

					switch($currency){
						case 'Kč' :
						case 'kč' : {
							$currency = 'CZK';
						}break;
						case '€' :
						case 'eur' :
						{
							$currency = 'EUR';
						}break;
						case 'PLN' :
						case 'pln' :
						{
							$currency = 'PLN';
						}break;
						case 'RUB' :
						case 'rub' :
						{
							$currency = 'RUB';
						}break;
						case 'GBP' :
						case 'gbp' :
						case '£' :
						{
							$currency = 'GBP';
						}break;
					}

					$currencies[] = $currency;

				}

				$c = array_count_values($currencies);
				if(count($c) === 0){
					$this->inappropriate = true;
					$this->inappropriateReason = 'No currency.';
					if($debug){
						echo '====: currency: '.$this->inappropriateReason;
						echo '<br>';
					}
					return null;
				}
				$currency = array_search(max($c), $c);

				if(!in_array($currency, CurrencyConvertor::$currencies, true)){
					$this->inappropriate = true;
					$this->inappropriateReason = 'No allowed currency.';
				}

				if($debug){
					echo '====: currency: '.$currency;
					echo '<br>';
				}
				return $currency;
			}

			if($debug){
				echo '====: currency: '.$this->inappropriateReason;
				echo '<br>';
			}
			return null;

		}

		protected static function compare($str1, $str2){

			//Odstraňujeme poslední písmenko z vyhledávané fráze
			$str1 = substr($str1, 0, -1);
			$str2 = substr($str2, 0, -1);

			similar_text($str1, $str2, $percent);

			if(levenshtein($str1, $str2) <= 2 and $percent >= 80 and self::string_compare($str1, $str2) > 0.78){
				return true;
			}

			return false;

		}

		protected static function string_compare($str_a, $str_b)
		{
			$length = strlen($str_a);
			$length_b = strlen($str_b);

			$i = 0;
			$segmentcount = 0;
			$segmentsinfo = array();
			$segment = '';
			while ($i < $length)
			{
				$char = substr($str_a, $i, 1);
				if (strpos($str_b, $char) !== FALSE)
				{
					$segment = $segment.$char;
					if (strpos($str_b, $segment) !== FALSE)
					{
						$segmentpos_a = $i - strlen($segment) + 1;
						$segmentpos_b = strpos($str_b, $segment);
						$positiondiff = abs($segmentpos_a - $segmentpos_b);
						$posfactor = ($length - $positiondiff) / $length_b; // <-- ?
						$lengthfactor = strlen($segment)/$length;
						$segmentsinfo[$segmentcount] = array( 'segment' => $segment, 'score' => ($posfactor * $lengthfactor));
					}
					else
					{
						$segment = '';
						$i--;
						$segmentcount++;
					}
				}
				else
				{
					$segment = '';
					$segmentcount++;
				}
				$i++;
			}

			// PHP 5.3 lambda in array_map
			$totalscore = array_sum(array_map(function($v) { return $v['score'];  }, $segmentsinfo));
			return $totalscore;
		}

	}