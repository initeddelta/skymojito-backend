<?php
/**
 * Project: agregator_letenek
 * File: SiteFactory.php
 * Author: Tomas Syrovy <syrovy.tom@gmail.com>
 * Date: 28.05.16
 * Version: 1.0
 */

namespace AppBundle\Factory;


use AppBundle\Entity\Site;

class SiteFactory {

	public function create(array $options){

		$entity = new Site();
		$entity->setCode($options['code']);
		$entity->setUri($options['uri']);

		return $entity;

	}

}