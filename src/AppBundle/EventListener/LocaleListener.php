<?php

namespace AppBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpFoundation\Cookie;

class LocaleListener implements EventSubscriberInterface
{
    private $defaultLocale;

    public function __construct($defaultLocale = 'cs')
    {
        $this->defaultLocale = $defaultLocale;
        $this->setCookie = false;
        $this->cookieLocale = null;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        
        $request = $event->getRequest();        
        
        if($locale = $request->get('_locale')) {
            
            $request->setLocale($locale);
            $request->getSession()->set('_locale', $locale);
            $this->setCookie = true;
            $this->cookieLocale = $locale;
                        
        } else {
            
            //check if user returns (has cookie)
            if($cookie = $request->cookies->get('locale')) {
                $request->setLocale($cookie);
                $request->getSession()->set('_locale', $cookie);
            } elseif($locale = $request->attributes->get('_locale')) {
                $request->getSession()->set('_locale', $locale);
            } else {
                //if no explicit locale has been set on this request, use one from the session
                $request->setLocale($request->getSession()->get('_locale', $this->defaultLocale));
            }
            
        }
        
    }
    
    public function onKernelResponse(FilterResponseEvent $event)
    {
        $response = $event->getResponse();
        
        if($this->setCookie) {
            $response->headers->setCookie(new Cookie('locale', $this->cookieLocale));
        }
    }

    public static function getSubscribedEvents()
    {
        return array(
            // must be registered before the default Locale listener
            KernelEvents::REQUEST => array(array('onKernelRequest', 17)),
            KernelEvents::RESPONSE => array(array('onKernelResponse', 17)),
        );
    }
}