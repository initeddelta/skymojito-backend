<?php

namespace AppBundle\Controller;

use AppBundle\Entity\DestinationFrom;
use AppBundle\Entity\DestinationUser;
use AppBundle\Entity\Locale;
use AppBundle\Entity\Post;
use AppBundle\Entity\Destination;
use AppBundle\EventSubscriber\LocaleSubscriber;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Gedmo\Tree\Entity\Repository\ClosureTreeRepository;
use MartinGeorgiev\SocialPost\Provider\Message;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\Request;

class WatchDogController extends Controller
{

	/**
     * @Route("/watch-dog", name="app.watchDog.index")
     * @Template()
     */
    public function indexAction(Request $request){

    	$user = $this->getUser();

    	if(!$user){
    		$this->addFlash('danger', 'Pro provedení této akce musíte být přihlášen.');
    		return $this->redirectToRoute('app.default.index');
	    }

	    $em = $this->getDoctrine()->getEntityManager();

	    if($request->request->has('form')){

	    	//Odstranění starých záznamů
		    $criteria = [
			    'user' => $user
		    ];
		    $dus = $em->getRepository(DestinationUser::class)->findBy($criteria);
		    foreach($dus as $du){
		    	$em->remove($du);
		    }
		    $em->flush();

		    //Uložení nových z DB
			$form = $request->request->get('form');
			$destinations = $form['destinations'];
			foreach($destinations as $d){
				$destination = $em->getRepository(Destination::class)->find($d);
				if($destination){
					$destinationUser = new DestinationUser();
					$destinationUser->setUser($user);
					$destinationUser->setDestination($destination);
					$em->persist($destinationUser);
				}
			}
		    $em->flush();

		    $this->addFlash('success', 'Nastavení hlídání akcí bylo uloženo.');

	    }

	    $regions = $this->getDestinations($em, ['01-continent'], $request->getLocale());
//	    $regions = $this->getDestinations($em, ['01-continent', '02-region'], $request->getLocale());
    	$countries = $this->getDestinations($em, ['04-country'], $request->getLocale());

    	$criteria = [
    		'user' => $user
	    ];
    	$dus2 = $em->getRepository(DestinationUser::class)->findBy($criteria);
    	$dus = [];
    	foreach($dus2 as $du2){
    		$dus[] = $du2->getDestination()->getId();
	    }

//    	$formBuilder = $this->createFormBuilder();
//    	$formBuilder->add('regions', ChoiceType::class, [
//    		'multiple' => true,
//		    'expanded' => true,
//		    'choices' => $regions,
//		    'choice_label' => 'title'
//	    ]);
//	    $formBuilder->add('countries', ChoiceType::class, [
//		    'multiple' => true,
//		    'expanded' => true,
//		    'choices' => $countries,
//		    'choice_label' => 'title'
//	    ]);

//	    $form = $formBuilder->getForm();

	    $data = [
	    	'regions' => $regions,
		    'countries' => $countries,
		    'dus' => $dus
	    ];

	    return $data;

   }

   private function getDestinations(EntityManagerInterface $em, $type, $localeName){

	   $dql = '
    	    SELECT dl
    	    FROM AppBundle:DestinationLocale dl
    	    JOIN dl.destination d
    	    JOIN dl.locale l
    	    WHERE d.type IN (:type)
    	    AND l.name = :localeName
    	    ORDER BY dl.title
    	';

	   $query = $em->createQuery($dql);
	   $query->setParameter('type', $type);
	   $query->setParameter('localeName', $localeName);

	   $result = $query->getResult();

	   return $result;

   }


}
