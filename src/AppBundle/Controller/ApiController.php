<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use AppBundle\PushNotification\PushNotifications;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ApiController extends Controller
{

	/**
	 * Get the list of posts.
	 *
	 * @param ParamFetcherInterface $paramFetcher
	 * @param string    $lang   language code of results
	 * @param string    $limit  number of results
	 *
	 * @Route("/api/post")
	 * @Rest\QueryParam(name="lang", requirements="[a-z]{2}", default="cs", description="Language of results.")
	 * @Rest\QueryParam(name="limit", requirements="\d+", default="200", description="Number of results.")
	 */
	public function getPostsAction(Request $request, ParamFetcherInterface $paramFetcher)
	{
		$lang = $paramFetcher->get('lang');
		$limit = $paramFetcher->get('limit');

		$em = $this->getDoctrine()->getManager();

		$posts = $em->getRepository(Post::class)->getPostsToShow($limit);

		$data = [];

		/** @var Post $post */
		foreach($posts as $post){

			$params = [
				'post_id' => $post->getId()
			];
			$url = $this->generateUrl('app.default.post', $params, UrlGeneratorInterface::ABSOLUTE_URL);
			$imgUrl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath().'/assets/destinations/' . $post->getDestinationTo()->getImageName();

			$d = [
				'id' => $post->getId(),
				'caption' => $post->getTitle($lang),
				'content' => $post->getPerex($lang),
				'url' => $url,
				'contentThumbnailUrl' => $imgUrl,
				'images' => [
					$imgUrl
				],
				'priority' => null,
				'publishTime' => $post->getCreatedAt()->getTimestamp(),
				'lang' => $lang,
				'tags' => [$post->getDestinationTo()->getTitle('en')],
			];

			$d = json_encode($d);
            $d = str_replace('"images":[', '"images":{"0":', $d);
            $d = str_replace('"tags":[', '"tags":{"0":', $d);
            $d = str_replace('[', '{', $d);
            $d = str_replace(']', '}', $d);
			$data[] = $d;

		}

		$result = '{"contents":{';
        $i = 1;
        foreach($data as $d){
            $result = $result.'"'.$i.'":'.$d.'';
            if($i < count($data)){
                $result = $result.',';
            }
            $i++;
        }
        $result = $result.'}}';

        return new Response($result);

	}

	/**
     * @Route("/api/device/register")
     */
    public function registerAction(Request $request)
    {
    	$em = $this->getDoctrine()->getManager();

    	$pn = new PushNotifications($em);

    	//TODO - ověřit, že jsou všechny paramatry

    	$data = [
    		'pnToken' => $request->query->get('pnToken'),
		    'user' => $this->getUser(),
		    'regTime' => new \DateTime(),
		    'type' => $request->query->get('type'),
		    'model' => $request->query->get('model'),
		    'os' => $request->query->get('os'),
	    ];

    	$entity = $pn->register($data);

    	if($entity){
    		$response = [
    			'status' => 200,
		    ];
	    }else{
    		$response = [
    			'status' => 500,
			    'message' => 'Error',
		    ];
	    }

	    return new JsonResponse($response, $response['status']);

    }

	/**
	 * @Route("/api/send-to-all")
	 */
	public function sendToAll(Request $request)
	{
		$em = $this->getDoctrine()->getManager();

		$pn = new PushNotifications($em);

		$title = 'TITLE';
		$message = 'MESSAGE';
		$id = 'ID';
		$type = 'TYPE';
		$actionsCount = 'ACTIONSCOUNT';
		
		$pn->sendToAll($title, $message, $id, $type, $actionsCount);

		$response = [
			'status' => 200,
		];

		return new JsonResponse($response, $response['status']);

	}

}
