<?php

namespace AppBundle\Controller;

use AppBundle\Entity\DestinationFrom;
use AppBundle\Entity\DestinationLocale;
use AppBundle\Entity\DestinationTo;
use AppBundle\Entity\Locale;
use AppBundle\Entity\Post;
use AppBundle\Entity\Destination;
use AppBundle\Entity\Site;
use AppBundle\Library\CurrencyConvertor;
use Gedmo\Tree\Entity\Repository\ClosureTreeRepository;
use MartinGeorgiev\SocialPost\Provider\Message;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/header", name="app.default.header")
     * @Template()
     */
    public function headerAction(Request $request){

    	$data = [];

        return $data;

    }

	/**
	 * @Route("/esky", name="app.default.esky")
	 * @Template()
	 */
	public function eskyAction(Request $request){

		$data = [];

		return $data;

	}

	/**
	 * @Route("/locale/{_locale}", name="app.default.index_locale")
	 *
	 */
	public function indexLocaleAction(Request $request)
	{
		return $this->redirectToRoute('app.default.index');
	}

	/**
	 * @Route("/post/social/test", name="app.default.socialTest")
	 * @Template()
	 *
	 */
	public function postSocialTestAction(Request $request){

		$message = new Message('Toto je testovací zpráva!', 'http://seznam.cz', 'https://www.mediaguru.cz/wp-content/uploads/2015/09/Seznam_HP.png');
		$published = $this->container->get('social_post')->publish($message);

		dump($published);exit;

	}

	/**
	 * @Route("/posts", name="app.default.posts")
	 * @Template()
	 *
	 */
	public function postsAction(Request $request){

		$sql = 'SELECT mp.id as mp_id, p.id as p_id, mp.processed, p.inappropriate, p.inappropriate_reason, mp.maybeFrom, mp.maybePageFrom, df.title as titleFrom, mp.maybeTo, mp.maybePageTo, dt.title as titleTo, mp.maybePrice, p.price, mp.maybeCurrency, p.currency, p.url
				FROM maybe_post mp
				LEFT JOIN post p ON p.maybePost_id = mp.id
				LEFT JOIN destination_locale dt ON p.destinationTo_id = dt.destination_id AND dt.locale_id = 1
				LEFT JOIN destination_locale df ON p.destinationFrom_id = df.destination_id AND df.locale_id = 1';

		$statement = $this->getDoctrine()->getManager()->getConnection()->prepare($sql);
		$statement->execute();

		$result = $statement->fetchAll();

		$data = [
			'result' => $result,
		];

		return $data;

	}

    /**
     * @Route("/", name="app.default.index")
     * @Route("/site/{code}", name="app.default.siteCode")
     *
     * @Template()
     */
    public function indexAction(Request $request, $code = null)
    {

        $em = $this->getDoctrine()->getManager();

        switch($request->getLocale()){
	        case 'cs' : {
	        	$domainCurrency = 'CZK';
	        	$formPrices = [
	        		2000 => 'do 2 000 CZK',
	        		5000 => 'do 5 000 CZK',
	        		10000 => 'do 10 000 CZK',
	        		15000 => 'do 15 000 CZK',
	        		999999999 => 'neomezeně',
		        ];
	        }break;
	        case 'sk' : {
		        $domainCurrency = 'EUR';
		        $formPrices = [
			        100 => 'do 100 EUR',
			        200 => 'do 200 EUR',
			        400 => 'do 400 EUR',
			        600 => 'do 600 EUR',
			        999999999 => 'bez obmedzenia',
		        ];
	        }break;
	        case 'pl' : {
		        $domainCurrency = 'PLN';
		        $formPrices = [
			        400 => 'do 400 PLN',
			        800 => 'do 800 PLN',
			        1500 => 'do 1 500 PLN',
			        2500 => 'do 2 500 PLN',
			        999999999 => 'bez limitów',
		        ];
	        }break;
	        case 'en' : {
		        $domainCurrency = 'EUR';
		        $formPrices = [
			        100 => 'under 100 EUR',
			        200 => 'under 200 EUR',
			        400 => 'under 400 EUR',
			        600 => 'under 600 EUR',
			        999999999 => 'unlimited',
		        ];
	        }break;
	        case 'de' : {
		        $domainCurrency = 'EUR';
		        $formPrices = [
			        100 => 'unter 100 EUR',
			        200 => 'unter 200 EUR',
			        400 => 'unter 400 EUR',
			        600 => 'unter 600 EUR',
			        999999999 => 'unbegrenzt',
		        ];
	        }break;
	        case 'ru' : {
		        $domainCurrency = 'RUB';
		        $formPrices = [
			        5000 => 'ниже 5 000 RUB',
			        10000 => 'ниже 10 000 RUB',
			        20000 => 'ниже 20 000 RUB',
			        30000 => 'ниже 30 000 RUB',
			        999999999 => 'без ограничения',
		        ];
	        }break;
	        default : {
		        $domainCurrency = 'CZK';
		        $formPrices = [
			        2000 => 'do 2 000 CZK',
			        5000 => 'do 5 000 CZK',
			        10000 => 'do 10 000 CZK',
			        15000 => 'do 15 000 CZK',
			        999999999 => 'neomezeně',
		        ];
	        }break;
        }

	    $rates = CurrencyConvertor::getRates($domainCurrency);

        $locale = $request->getLocale();

	    $sql = "
    	    SELECT d2.id, destination_closure.ancestor, d2.level, dl2.title 
    	    FROM destination d1 
    	    LEFT JOIN destination_closure ON d1.id = destination_closure.ancestor 
    	    LEFT JOIN destination d2 ON destination_closure.descendant = d2.id 
    	    LEFT JOIN destination_locale dl2 ON dl2.destination_id = d2.id 
    	    LEFT JOIN locale l2 ON l2.id = dl2.locale_id 
    	    LEFT JOIN destination_locale dl1 ON dl1.destination_id = d1.id 
    	    LEFT JOIN locale l1 ON l1.id = dl1.locale_id 
    	    WHERE d1.type = \"01-continent\" 
    	    	AND (d2.type = \"04-country\" OR d2.type = \"01-continent\") 
    	    	AND d2.alternative = 0 
    	    	AND l1.name = '".$locale."' 
    	    	AND l2.name = '".$locale."' 
            ORDER BY dl1.title, d2.level, dl2.title
    	";

	    $stmt = $em->getConnection()->prepare($sql);
	    $stmt->execute();
	    $destinations = $stmt->fetchAll();

        $posts = $em->getRepository(Post::class)->getPostsToShow();

        if($code !== null){
        	$criteria = [
        		'code' => $code,
	        ];
        	$site = $em->getRepository(Site::class)->findOneBy($criteria);
        	if($site){
        		$posts = array_filter($posts, function($entry) use($site){
        			/** @var \AppBundle\Entity\PostSite $postSite */
			        foreach($entry->getPostSites() as $postSite){
        				if($postSite->getSite() === $site){
        					return true;
				        }
			        }
        			return false;
		        });
	        }
        }

	    $f = [];

	    /** @var ClosureTreeRepository $destinationRepository */
        $destinationRepository = $em->getRepository(Destination::class);

        /** @var DestinationLocale $destinationLocaleRepository */
        $destinationLocaleRepository = $em->getRepository(DestinationLocale::class);

        $destinationFromRepository = $em->getRepository(DestinationFrom::class);

        $formExtend = false;

        $locale = $request->getLocale();
        $criteria = [
            'name' => $locale,
        ];
        $localeEntity = $em->getRepository(Locale::class)->findOneBy($criteria);

	    if($request->request->has('destination-to')){

            $formExtend = true;

		    $dts = explode(';', $request->request->get('destination-to'));

		    $values = [];
		    $no_dts = [];

		    $query = 'dt_';
		    foreach($dts as $dt){
		        if(substr($dt, 0, strlen($query)) === $query){
		            $destinationTo_String = str_replace($query, '', $dt);
		            $criteria = [
		                'title' => $destinationTo_String,
                        'locale' => $localeEntity
                    ];
		            $destinationTo = $this->getDoctrine()->getRepository(DestinationTo::class)->findOneBy($criteria);
		            $destinationToDestinations = $destinationTo->getDestinations();
		            foreach($destinationToDestinations as $destination){
                        $values[] = $destination->getTitle($locale);
                    }
                }else{
		            $values[] = $dt;
		            $no_dts[] = $dt;
                }
            }

		    if(count($values) > 0){

			    $result = [];

			    foreach($values as $value){

				    $criteria = [
					    'title' => $value,
				    ];
				    /** @var DestinationLocale $destinationLocale */
				    $destinationLocale = $destinationLocaleRepository->findOneBy($criteria);
				    $destination = $destinationLocale->getDestination();

				    if($destination){
					    $result = array_merge($destinationRepository->children($destination, false, null, 'ASC', true), $result);
				    }

			    }

			    if(count($result) > 0){

				    $posts = array_filter($posts, function($entry) use ($result) {
					    return in_array($entry->getDestinationTo(), $result, true);
				    });

			    }

		    }
		    $f['destination-to'] = array_merge($dts, $no_dts);

	    }

	    if($request->request->has('destination-from')){

            $formExtend = true;

		    $values = explode(';', $request->request->get('destination-from'));
		    if(count($values) > 0){

			    $result = [];

			    foreach($values as $value){

				    $criteria = [
					    'title' => $value,
				    ];
				    /** @var DestinationLocale $destinationLocale */
				    $destinationLocale = $destinationLocaleRepository->findOneBy($criteria);
				    $destination = $destinationLocale->getDestination();

				    if($destination){
					    $result = array_merge($destinationRepository->children($destination, false, null, 'ASC', true), $result);
				    }

			    }

			    if(count($result) > 0){

				    $posts = array_filter($posts, function($entry) use ($result) {
					    return in_array($entry->getDestinationFrom(), $result, true);
				    });

			    }

		    }
		    $f['destination-from'] = $values;

	    }

        if($request->request->has('simple-to')){

        	$values = explode(';', $request->request->get('simple-to'));
        	if(count($values) > 0){

        		$result = [];

        		foreach($values as $value){

			        $criteria = [
				        'title' => $value,
			        ];
			        /** @var DestinationLocale $destinationLocale */
			        $destinationLocale = $destinationLocaleRepository->findOneBy($criteria);
			        $destination = $destinationLocale->getDestination();

			        if($destination){
				        $result = array_merge($destinationRepository->children($destination, false, null, 'ASC', true), $result);
			        }

		        }

		        if(count($result) > 0){

			        $posts = array_filter($posts, function($entry) use ($result) {
				        return in_array($entry->getDestinationTo(), $result, true);
			        });

		        }

	        }
	        $f['simple-to'] = $values;

        }

	    if($request->request->has('simple-from')){

		    $values = explode(';', $request->request->get('simple-from'));
		    if(count($values) > 0){

			    $result = [];

			    foreach($values as $value){

				    $criteria = [
					    'title' => $value,
				    ];
				    $destinationFrom = $destinationFromRepository->findOneBy($criteria);

				    if($destinationFrom){
					    $result[] = $destinationFrom;
				    }

			    }


			    if(count($result) > 0){

				    $posts = array_filter($posts, function($entry) use ($result) {
				    	if($entry->getDestinationFrom() and $entry->getDestinationFrom() !== null){
				    		foreach($result as $r){
				    			if(in_array($entry->getDestinationFrom(), $r->getDestinations()->toArray(), true)){
				    				return true;
							    }
						    }
					    }
					    return false;
				    });

			    }

		    }
		    $f['simple-from'] = $values;

	    }

	    if($request->request->has('simple-price')){

		    $values = explode(';', $request->request->get('simple-price'));
		    if(count($values) > 0){

		    	foreach($values as $value){
				    $posts = array_filter($posts, function($entry) use ($value, $rates) {
				    	if(in_array($entry->getCurrency(), CurrencyConvertor::$currencies)){
							$price = $entry->getPrice()*$rates[$entry->getCurrency()];
						    return $price <= $value;
					    }
					    return false;
				    });
			    }

		    }
		    $f['simple-price'] = $values;

	    }

	    $criteria = [
	    	'type' => '01-continent',
	    ];
	    $orderBy = [
	    	'id' => 'ASC',
	    ];
	    $fds = $em->getRepository(Destination::class)->findBy($criteria, $orderBy);
	    $formDestinations = [];
	    foreach($fds as $fd){
	    	if($fd->getTitle($locale) !== null){
			    $formDestinations[$fd->getTitle($locale)] = $fd->getTitle($locale);
		    }
	    }

	    $criteria = [
	    	'locale' => $localeEntity,
	    ];
	    $formDestinationFroms = $em->getRepository(DestinationFrom::class)->findBy($criteria);

	    $ids = [];
	    /** @var Post $post */
	    foreach($posts as $post){
	    	$ids[] = $post->getId();
	    }

        $criteria = [
            'locale' => $localeEntity,
        ];
        $formDestinationTos = $em->getRepository(DestinationTo::class)->findBy($criteria);

        $data = [
            'posts' => $posts,
	        'ids' => implode(',', $ids),
	        'destinations' => $destinations,
	        'f' => $f,
	        'domainCurrency' => $domainCurrency,
	        'formDestinations' => $formDestinations,
	        'formDestinationFroms' => $formDestinationFroms,
	        'formDestinationTos' => $formDestinationTos,
	        'formPrices' => $formPrices,
            'formExtend' => $formExtend,
        ];

        return $data;

    }

	/**
	 * @Route("/posts/render", name="app.default.renderPosts")
	 */
    public function renderPostsAction(Request $request){

    	$ids = $request->request->get('ids');
		$offset = $request->request->get('offset');
		$domainCurrency = $request->request->get('domainCurrency');

		$c = count(explode(',', $ids));

	    $rates = CurrencyConvertor::getRates($domainCurrency);

	    $ids = array_slice(explode(',', $ids), $offset*12*2, 24);

	    $em = $this->getDoctrine()->getEntityManager();
	    $qb = $em->createQueryBuilder();
	    $qb->select('p')->from('AppBundle:Post', 'p')->where('p.id IN ('.implode(',', $ids).')')->orderBy('p.id', 'DESC');
	    $posts = $qb->getQuery()->getResult();

	    $params = [
		    'posts' => $posts,
		    'rates' => $rates,
		    'domainCurrency' => $domainCurrency,
	    ];

	    $html = $this->renderView('AppBundle:Default:_posts.html.twig', $params);
	    $data = [
		    'html' => $html,
		    'showBtn' => $c-($offset+1)*12*2 > 0 ? 1 : 0,
	    ];

		return new JsonResponse($data);

    }

    /**
     * @Route("/post/{post_id}", name="app.default.post")
     * @Template()
     */
    public function postAction(Request $request, $post_id)
    {

        $em = $this->getDoctrine()->getManager();

	    switch($request->getLocale()){
		    case 'cs' : {
			    $domainCurrency = 'CZK';
			    $formPrices = [
				    2000 => 'do 2 000 CZK',
				    5000 => 'do 5 000 CZK',
				    10000 => 'do 10 000 CZK',
				    15000 => 'do 15 000 CZK',
				    999999999 => 'neomezeně',
			    ];
		    }break;
		    case 'sk' : {
			    $domainCurrency = 'EUR';
			    $formPrices = [
				    100 => 'do 100 EUR',
				    200 => 'do 200 EUR',
				    400 => 'do 400 EUR',
				    600 => 'do 600 EUR',
				    999999999 => 'bez obmedzenia',
			    ];
		    }break;
		    case 'pl' : {
			    $domainCurrency = 'PLN';
			    $formPrices = [
				    400 => 'do 400 PLN',
				    800 => 'do 800 PLN',
				    1500 => 'do 1 500 PLN',
				    2500 => 'do 2 500 PLN',
				    999999999 => 'bez limitów',
			    ];
		    }break;
		    case 'en' : {
			    $domainCurrency = 'EUR';
			    $formPrices = [
				    100 => 'under 100 EUR',
				    200 => 'under 200 EUR',
				    400 => 'under 400 EUR',
				    600 => 'under 600 EUR',
				    999999999 => 'unlimited',
			    ];
		    }break;
		    case 'de' : {
			    $domainCurrency = 'EUR';
			    $formPrices = [
				    100 => 'unter 100 EUR',
				    200 => 'unter 200 EUR',
				    400 => 'unter 400 EUR',
				    600 => 'unter 600 EUR',
				    999999999 => 'unbegrenzt',
			    ];
		    }break;
		    case 'ru' : {
			    $domainCurrency = 'RUB';
			    $formPrices = [
				    5000 => 'ниже 5 000 RUB',
				    10000 => 'ниже 10 000 RUB',
				    20000 => 'ниже 20 000 RUB',
				    30000 => 'ниже 30 000 RUB',
				    999999999 => 'без ограничения',
			    ];
		    }break;
		    default : {
			    $domainCurrency = 'CZK';
			    $formPrices = [
				    2000 => 'do 2 000 CZK',
				    5000 => 'do 5 000 CZK',
				    10000 => 'do 10 000 CZK',
				    15000 => 'do 15 000 CZK',
				    999999999 => 'neomezeně',
			    ];
		    }break;
	    }

	    $rates = CurrencyConvertor::getRates($domainCurrency);

        $post = $em->getRepository(Post::class)->find($post_id);

        if(!$post){

            return $this->redirectToRoute('app.default.index');

        }

        $data = [
            'post' => $post,
	        'domainCurrency' => $domainCurrency,
	        'rates' => $rates,
        ];

        return $data;

    }


}
