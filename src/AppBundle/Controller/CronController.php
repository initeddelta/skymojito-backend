<?php

	namespace AppBundle\Controller;

	use AppBundle\Entity\Destination;
	use AppBundle\Entity\Locale;
	use AppBundle\Entity\MaybePost;
	use AppBundle\Entity\Post;
	use AppBundle\Entity\PostLocale;
	use AppBundle\Entity\PostSite;
	use AppBundle\Entity\Site;
	use AppBundle\Entity\Social;
	use AppBundle\Entity\SocialPost;
	use AppBundle\Library\CurrencyConvertor;
	use AppBundle\Library\Library;
	use AppBundle\NotificationManager\NotificationManager;
	use AppBundle\NotificationManager\NotificationTypeLibrary;
	use AppBundle\Parser\Parser;
	use AppBundle\SocialPost\FacebookProvider;
	use AppBundle\SocialPost\FacebookProviderBuilder;
	use AppBundle\SocialPost\Provider;
	use AppBundle\SocialPost\SocialPoster;
	use Doctrine\Common\Persistence\ObjectManager;
	use Doctrine\ORM\EntityNotFoundException;
	use Doctrine\ORM\QueryBuilder;
	use Facebook\Facebook;
	use MartinGeorgiev\SocialPost\Provider\Facebook\SDK5;
	use MartinGeorgiev\SocialPost\Provider\Message;
	use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
	use simplehtmldom_1_5\simple_html_dom;
	use Symfony\Bundle\FrameworkBundle\Controller\Controller;
	use Symfony\Component\Debug\Exception\ContextErrorException;
	use Symfony\Component\HttpFoundation\Request;
	use Symfony\Component\HttpFoundation\Response;
	use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

	class CronController extends Controller
	{
		/** @var ObjectManager */
		private $em;

		private static $maxMedias = 3;
		private static $maxMaybePosts = 1;

		public function init(Request $request){
			$this->em = $this->getDoctrine()->getManager();

			require_once __DIR__.'/../../php-simple-html-dom-parser/Src/Sunra/PhpSimple/HtmlDomParser.php';
		}

		/**
		 * @Route("/social/publish/{code}")
		 */
		public function socialPublishAction(Request $request, $code){

			$this->init($request);

			$socialPoster = new SocialPoster($this->em, $request, $this->container);

			$response = $socialPoster->publish($code);

			return new Response($response, 200);

		}

		/**
		 * @Route("/crawle/wiki/{offset}")
		 */
		public function wikiAction(Request $request, $offset = 0)
		{

			$this->init($request);

			$destinations = $this->em->getRepository(Destination::class)->findBy([], [], 100, $offset);

			/** @var Destination $destination */
			foreach($destinations as $destination){

				try{

					$html = new simple_html_dom();
					$niceName = str_replace(' ', '_', mb_convert_case($destination->getTitle(), MB_CASE_TITLE));
					$url = 'https://cs.wikipedia.org/wiki/'.$niceName;
					$html->load_file($url);

					if(!empty($html)){
						$info = $html->find( '#bodyContent p', 0 );
						if( $info ){
							$info = strip_tags( $info->plaintext );
							$destination->setInfo($info);
							$this->em->persist($destination);
						}
					}

				}catch(ContextErrorException $e){}

				$this->em->flush();

			}

			return new Response('200: Successfull!', 200);

		}

		/**
		 * @Route("/crawle/{siteCode}")
		 */
		public function crawleAction(Request $request, $siteCode)
		{

			$this->init($request);

			$criteria = [
				'code' => $siteCode,
			];
			$site = $this->em->getRepository(Site::class)->findOneBy($criteria);

			if(!$site){
				throw new EntityNotFoundException();
			}

			$uri = $site->getUri();
			$fcontent = null;

			if($site->getCrawleMethod() === 'file_get_contents'){

				$fcontent = file_get_contents($uri);

			}

			if($site->getCrawleMethod() === 'curl'){

				// create curl resource
				$ch = curl_init();

				// set url
				curl_setopt($ch, CURLOPT_URL, $uri);

				//return the transfer as a string
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

				// $output contains the output string
				$fcontent = curl_exec($ch);

				// close curl resource to free up system resources
				curl_close($ch);

			}

			if($fcontent === null){

				return new Response('500: Bad crawle method for that site!', 500);

			}

			$html = new simple_html_dom();
			$html->load($fcontent);

			$medias = $html->find($site->getPathToMedias());

			if($medias){

				$medias = array_slice($medias, 0, self::$maxMedias);

				foreach($medias as $media){

					$maybeUrl = $media;
					if(trim($site->getPathToURL())){
						$maybeUrl = $media->find($site->getPathToURL(), 0);
					}

					if($maybeUrl){

						$url = null;
						$crawlePage = true;

						if($site->getPathToURLSource() === 'plaintext'){
							$url = $maybeUrl->plaintext;
							$crawlePage = true;
						}
						if($site->getPathToURLSource() === 'href'){
							$url = $maybeUrl->href;
							$crawlePage = true;
						}
						if($site->getPathToURLSource() === 'href - full'){
							$url = $maybeUrl->href;
							if($site->getCode() === 'kadetade.com'){
								$url = 'https://kadetade.com'.$url;
							}else{
								$url = $site->getUri().$url;
							}
							$crawlePage = true;
						}
						if($site->getPathToURLSource() === 'site'){
							$url = $site->getUri();
							$crawlePage = false;
						}

						if (strpos($url, '//') === 0) {
							$url = 'http:'.$url;
						}

						if($url){

							$maybePost = new MaybePost();
							$maybePost->setMaybeUrl($maybeUrl);
							$maybePost->setUrl($url);
							$maybePost->setSite($site);
							$maybePost->setCreatedAt(new \DateTime());
							$maybePost->setProcessed(false);

							$maybeTo = null;

							$maybeTo_F = $media->find($site->getPathToDestinationTo(), 0);
							if($maybeTo_F){
								$maybeTo = $maybeTo_F->plaintext;
								if($maybeTo){
									$maybePost->setMaybeTo($maybeTo);
								}
							}

							$criteria = [
								'url' => $url,
								'maybeTo' => $maybeTo,
							];
							/** @var MaybePost $maybePostOther */
							$maybePostOther = $this->em->getRepository(MaybePost::class)->findOneBy($criteria);

							if($maybePostOther){ //pokud existuje už maybePost
								continue;
							}

							$maybeFrom_F = $media->find($site->getPathToDestinationFrom(), 0);
							if($maybeFrom_F){
								$maybeFrom = $maybeFrom_F->plaintext;
								if($maybeFrom){
									$maybePost->setMaybeFrom($maybeFrom);
								}
							}

							$maybePrice_F = $media->find($site->getPathToPrice(), 0);
							if($maybePrice_F){
								$maybePrice = $maybePrice_F->plaintext;
								if($maybePrice){
									$maybePost->setMaybePrice($maybePrice);
								}
							}

							$maybeCurrency_F = $media->find($site->getPathToCurrency(), 0);
							if($maybeCurrency_F){
								$maybeCurrency = $maybeCurrency_F->plaintext;
								if($maybeCurrency){
									$maybePost->setMaybeCurrency($maybeCurrency);
								}
							}

							$continue = $crawlePage;
							$pageContent = null;

							if($site->getCrawleMethod() === 'file_get_contents'){

								try {
									$pageContent = file_get_contents($url);
								}catch(ContextErrorException $e){
									$continue = false;
								}


							}

							if($site->getCrawleMethod() === 'curl'){

								// create curl resource
								$ch = curl_init();

								// set url
								curl_setopt($ch, CURLOPT_URL, $url);

								//return the transfer as a string
								curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
								curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

								// $output contains the output string
								$pageContent = curl_exec($ch);

								// close curl resource to free up system resources
								curl_close($ch);

							}

							if($continue){

								if($pageContent === null){

									return new Response('500: Bad crawle method for that site!', 500);

								}

								$pageHtml = new simple_html_dom($pageContent);

								if($pageHtml){

									$maybePageFrom_F = $pageHtml->find($site->getPathToPageDestinationFrom(), 0);
									if($maybePageFrom_F){
										$maybePageFrom = $maybePageFrom_F->plaintext;
										if($maybePageFrom){
											$maybePost->setMaybePageFrom($maybePageFrom);
										}
									}

									$maybePageTo_F = $pageHtml->find($site->getPathToPageDestinationTo(), 0);
									if($maybePageTo_F){
										$maybePageTo = $maybePageTo_F->plaintext;
										if($maybePageTo){
											$maybePost->setMaybePageTo($maybePageTo);
										}
									}

								}

							}

							$this->em->persist($maybePost);

						}

					}

				}

				$this->em->flush();

			}

			return new Response('200: Successfull!', 200);

		}

		/**
		 * @Route("/post/generate/")
		 * @Route("/post/generate/{maybePost_id}")
		 */
		public function postGenerateAction(Request $request, $maybePost_id = null)
		{

			$this->init($request);

			if($maybePost_id === null){

				$criteria = [
					'processed' => false,
				];
				$orderBy = [
					'id' => 'ASC',
				];
				$limit = self::$maxMaybePosts;
				$maybePosts = $this->em->getRepository(MaybePost::class)->findBy($criteria, $orderBy, $limit);
				$debug = false;

			}else{

				$maybePost = $this->em->getRepository(MaybePost::class)->find($maybePost_id);
				if(!$maybePost){
					throw new EntityNotFoundException();
				}
				$maybePosts = [
					$maybePost,
				];
				$debug = true;

			}

			/** @var MaybePost $maybePost */
			foreach($maybePosts as $maybePost){

				//pro jistotu ulož zpracování možná-příspěvku už nyní
				$maybePost->setProcessed(true);
				$this->em->persist($maybePost);
				$this->em->flush();

				//Check forbidden words
				$forbidden = false;
				foreach(explode(' ', $maybePost->getMaybeFrom()) as $w){
					if(in_array($w, Library::$forbiddenWords, false)){
						$forbidden = true;
						break;
					}
				}
				foreach(explode(' ', $maybePost->getMaybeTo()) as $w){
					if(in_array($w, Library::$forbiddenWords, false)){
						$forbidden = true;
						break;
					}
				}
				if(!$forbidden){

					$parser = new Parser($this->em, $maybePost);

					$post = $parser->createAndGetPost($debug);

					/** @var Post $similarPost */
					$similarPost = $this->findSimilarPost($post);

					if($similarPost){

						$titles = [];
						/** @var PostSite $postSite */
						foreach($similarPost->getPostSites() as $postSite){
							$titles[] = $postSite->getSite()->getTitle();
						}
						if(in_array($maybePost->getSite()->getTitle(), $titles, false)){
							continue;
						}

						$similarPost->setPrice($this->calculatePrice($similarPost, $post));

						$postSite = new PostSite();
						$postSite->setPosition(0);
						$postSite->setPost($similarPost);
						$postSite->setSite($maybePost->getSite());
						$postSite->setUrl($post->getUrl());

						$similarPost->addPostSite($postSite);

						$this->em->persist($similarPost);
						$this->em->persist($postSite);

					}else{

						//TODO - tady vzniká nový neduplicitní post

						//Vytvoř perex
						$destination = $post->getDestinationTo();
						if($destination){
							/** @var \AppBundle\Entity\DestinationLocale $destinationLocale */
							foreach($destination->getDestinationLocales() as $destinationLocale){
								if(!$destinationLocale->getInfo()){

									try{

										$localeName = $destinationLocale->getLocale()->getName();

										$html = new simple_html_dom();
										$niceName = str_replace(' ', '_', mb_convert_case($destination->getTitle($localeName), MB_CASE_TITLE));
										$url = 'https://'.$localeName.'.wikipedia.org/wiki/'.$niceName;
										$html->load_file($url);

										if(!empty($html)){
											$info = $html->find( '#bodyContent p', 0 );
											if($info){
												$info = strip_tags( $info->plaintext );
												$destinationLocale->setInfo($info);
												$this->em->persist($destinationLocale);
											}
										}

									}catch(ContextErrorException $e){}

								}

								$postLocale = new PostLocale();
								$postLocale->setLocale($destinationLocale->getLocale());
								$postLocale->setPost($post);
								$postLocale->setPerex($destinationLocale->getInfo());

								$this->em->persist($postLocale);

							}

						}
						//konec tvorby perexu

						$postSite = new PostSite();
						$postSite->setPosition(0);
						$postSite->setPost($post);
						$postSite->setSite($maybePost->getSite());
						$postSite->setUrl($post->getUrl());

						$post->addPostSite($postSite);

						$this->em->persist($post);
						$this->em->persist($postSite);

					}

					//TODO - po vytvoření příspěvku - zjisti, zda-li je validní a pošli do incomakeru

				}

				//na konci zpracování ulož, zda-li bylo zpracováno
				$maybePost->setProcessedEnd(true);
				$this->em->persist($maybePost);

				if($debug){
					exit;
				}

				$this->em->flush();

			}

			return new Response('200: Successfull!', 200);

		}



		private function findSimilarPost(Post $post){

			$em = $this->getDoctrine()->getManager();

			$createdAt = clone ($post->getCreatedAt());

			$qb = new QueryBuilder($em);
			$qb ->select('p')
			    ->from(Post::class, 'p')
//			    ->where('p.currency = :currency')
			    ->where('p.destinationFrom = :destinationFrom')
			    ->andWhere('p.destinationTo = :destinationTo')
			    ->andWhere('p.priceInEUR >= :priceMin')
			    ->andWhere('p.priceInEUR <= :priceMax')
			    ->andWhere('p.createdAt >= :createdAt');
			$qb->setParameters([
//				'currency' => $post->getCurrency(),
				'destinationFrom' => $post->getDestinationFrom(),
				'destinationTo' => $post->getDestinationTo(),
				'priceMin' => $post->getPriceInEUR()*0.8,
				'priceMax' => $post->getPriceInEUR()*1.2,
				'createdAt' => $createdAt->sub(new \DateInterval('P20D')),
			]);

			$result = $qb->getQuery()->setMaxResults(1)->getResult();

			if(array_key_exists(0, $result)){
				return $result[0];
			}

			return null;

		}

		private function calculatePrice(Post $similarPost, Post $post){

			return min($similarPost->getPrice(), $post->getPrice());

		}
	}