-- Získej všechny posty, které se objevily na CS serverech, které byly vytvořeny před 5 hodinami (tento den 8:00 - 5h), a které nebyly již zveřejněny na daných stránkách.

SELECT *
FROM post
  JOIN post_site ON post.id = post_site.post_id
  JOIN site ON post_site.site_id = site.id
  JOIN locale ON site.locale_id = locale.id
WHERE locale.name = 'cs'
  AND post.inappropriate = 0
  AND post.created_at >= '2017-12-07 03:00:00'
  AND post.id NOT IN (
    SELECT post_id
    FROM social_post
      JOIN social ON social.id = social_post.social_id
      WHERE social.code = 'facebook_cs'
  )
  AND post.currency IS NOT NULL
  AND post.price IS NOT NULL
  AND post.destinationTo_id IS NOT NULL
  AND post.destinationFrom_id IS NOT NULL
ORDER BY post.id DESC;