SELECT p.inappropriate, p.inappropriate_reason, mp.maybeFrom, df.title, mp.maybeTo, dt.title, mp.maybePrice, p.price, mp.maybeCurrency, p.currency, p.url
FROM post p
JOIN maybe_post mp ON p.maybePost_id = mp.id
JOIN destination dt ON p.destinationTo_id = dt.id
LEFT JOIN destination df ON p.destinationFrom_id = df.id