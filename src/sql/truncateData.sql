SET foreign_key_checks = 0;

TRUNCATE TABLE `maybe_post`;
TRUNCATE TABLE `post`;
TRUNCATE TABLE `post_locale`;
TRUNCATE TABLE `post_site`;
TRUNCATE TABLE `social_post`;