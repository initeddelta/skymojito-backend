-- Seznam všech kandidátů na příspěvek, u kterých bylo zahájeno zpracování, ale nemají vytvořený příspěvěk
SELECT * FROM maybe_post WHERE processed = 1 AND id NOT IN (SELECT maybePost_id FROM post);

-- Aktualizace všech kandidátů na příspěvek, u kterých bylo zahájeno zpracování, ale nemají vytvořený příspěvěk
UPDATE maybe_post SET processed = 0 WHERE processed = 1 AND id NOT IN (SELECT maybePost_id FROM post);