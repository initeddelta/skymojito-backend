<?php

namespace UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	public function __construct()
	{
		parent::__construct();
		// your own logic
	}

//	public function setUsername( $username ){
//		$this->setEmail($username);
//		return parent::setUsername( $username );
//	}
//
//	public function setUsernameCanonical( $usernameCanonical ){
//		$this->setEmailCanonical($usernameCanonical);
//		return parent::setUsernameCanonical( $usernameCanonical );
//	}
}
